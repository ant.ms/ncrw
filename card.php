<?php
    require 'partials/startup.php';
    include 'partials/functions/sexToString.php';
    include 'partials/functions/categoryToString.php';
    include 'partials/functions/humanTiming.php';

    // get data of animal
    $cmd = $db->get("SELECT * FROM Animal WHERE ncrwRefrenceNumber=?", [$_GET['animal']]);
    while ($row = $cmd->fetch()):
?>

<table>
    <tr>
        <th colspan="1">Type:</td>
        <td colspan="6"><?= categoryToString($db->getColumn("SELECT cate FROM Species WHERE speciesId=?", [$row['species']])) ?></td>
    </tr>
    <tr>
        <th colspan="1">Species:</td>
        <td colspan="6"><?= $db->getColumn("SELECT speciesName FROM Species WHERE speciesId=?", [$row['species']]) ?> (<?= $db->getColumn("SELECT speciesCommonName FROM Species WHERE speciesId=?", [$row['species']]) ?>)</td>
    </tr>
    <tr>
        <th colspan="1">ID-No:</td>
        <td colspan="3"><?= $row['ncrwRefrenceNumber'] ?></td>
        <th colspan="1">Sex:</td>
        <td colspan="3"><?= sexToLongString($row['sex']) ?></td>
    </tr>
    <tr>
        <th colspan="1">Name:</td>
        <td colspan="6"><?= $row['animalName'] ?></td>
    </tr>

    <tr>
        <th colspan="1">Date In:</td>
        <td colspan="3"><?= $row['dateOfEntry'] ?></td>
        <td colspan="3" rowspan="4" style="background: url(https://chart.googleapis.com/chart?cht=qr&chl=<?= $_SERVER['SERVER_NAME'] ?>%2F%3Fview%3D<?= $row['ncrwRefrenceNumber'] ?>&chs=180x180&choe=UTF-8&chld=L|2);background-position: center; background-size:contain;background-repeat:no-repeat"></td>
    </tr>
    <tr>
        <th colspan="1">Age:</td>
        <td colspan="3"><?= humanTiming(strtotime($row['dateOfBirth'])) ?></td>
    </tr>

    <tr>
        <th colspan="1">Bite Risk:</td>
        <td colspan="3"><?= $row['biterisk'] ?></td>
    </tr>
    <tr>
        <th colspan="1">Notes:</td>
        <td colspan="3"><?= $row['notes'] ?></td>
    </tr>
</table>

<?php endwhile; ?>


<style>
table {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 13cm;
}

td, th {
  border: 0.5mm solid #ddd;
  padding: 2mm;
}

th {
  padding-top: 2mm;
  padding-bottom: 2mm;
  text-align: left;
}
</style>

<script>
    window.addEventListener('load', function () {
        window.print()
    })
</script>

<?php $db->log("Card was accessed for ". $_GET['animal']) ?>