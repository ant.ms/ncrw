<?php 
    include 'partials/functions/sexToString.php';

    if (isset($_GET['revise'])) {
        $db->set("DELETE FROM mortality WHERE referenceNumber=?", [$_GET['revise']]);
        $db->set("DELETE FROM rehoming WHERE referenceNumber=?", [$_GET['revise']]);
        $db->log("Revised animal '" . $_GET['revise'] . "'", 1);

        echo "<script>history.pushState({}, '', '.?animals');</script>";
    }

    if (isset($_POST['modalRehomeAnimal'])) {
        $animal = $_POST['modalRehomeAnimal'];

        // TODO: Make secure
        $countryRaw = $_POST['countryCode'];
        
        $db->log("Rehomed animal: '$animal'", 1);
        $db->insert("rehoming", [
            'referenceNumber' => $animal,
            'existingConditions' => $_POST['existingConditions'],
            'countryCode' => json_decode(file_get_contents("https://restcountries.eu/rest/v2/name/$countryRaw?fullText=true"))[0]->alpha2Code,
            'dateOfDeparture' => ($_POST['dateOfDeparture'] != "" ? $_POST['dateOfDeparture'] : date(DATE_ATOM)),
            'checklist' => isset($_POST['checklist']),
            'donation' => $_POST['donation'],
            'newOwnerName' => $_POST['newOwnerName'],
            'newOwnerContactDetails' => $_POST['newOwnerContactDetails'],
            'username' => $_POST['personell']
        ]);

        echo "<script>document.addEventListener('DOMContentLoaded', function() {M.toast({html: 'Rehomed animal \'$animal\''});});</script>";
    }

    if (isset($_POST['modalMortalityAnimal'])) {
        $animal = $_POST['modalMortalityAnimal'];
        $db->log("Marked animal: '$animal' as dead", 1);
        $db->insert("mortality", [
            'referenceNumber' => $animal,
            'dateOfDeath' => ($_POST['dateOfDeath'] != "" ? $_POST['dateOfDeath'] : date(DATE_ATOM)),
            'deathMethod' => $_POST['deathMethod'],
            'dateOfDeparture' => ($_POST['dateOfDeparture'] != "" ? $_POST['dateOfDeparture'] : date(DATE_ATOM)),
            'causeOfDeath' => $_POST['causeOfDeath'],
            'vetenerinaryCare' => isset($_POST['vetenerinaryCare']),
            'postMortem' => isset($_POST['postMortem']),
            'cadaverStored' => isset($_POST['cadaverStored']),
            'notes' => $_POST['notes'],
            'username' => $_POST['personell']
        ]);

        echo "<script>document.addEventListener('DOMContentLoaded', function() {M.toast({html: 'Marked animal \'$animal\' as dead'});});</script>";
    }

    if (isset($_POST['modalWeightAnimal']))
        $db->insertMeasurement($_POST['modalWeightAnimal'], "WeightMeasurement", [
            'isApproximate' => isset($_POST['approximate']),
            'recordedWeight' => $_POST['weight']
        ]);

    if (isset($_POST['modalLengthAnimal']))
        $db->insertMeasurement($_POST['modalLengthAnimal'], "LengthMeasurement", [
            'isApproximate' => isset($_POST['approximate']),
            'recordedLength' => $_POST['length']
        ]);

    if (isset($_POST['modalCleanedAnimal']))
        $db->insertMeasurement($_POST['modalCleanedAnimal'], "CleanedMeasurement", []);

    if (isset($_POST['modalFedAnimal']))
        $db->insertMeasurement($_POST['modalFedAnimal'], "FedMeasurement", []);

    if (isset($_POST['modalTreatedAnimal']))
        $db->insertMeasurement($_POST['modalTreatedAnimal'], "TreatmentMeasurement", ['notes' => $_POST['notes']]);
?>