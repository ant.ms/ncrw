<!-- Dropdown Structure -->
<ul id='dropdownBookOut' class='dropdown-content'>
    <li>
        <a class="modal-trigger" href="#modalMortality">Mark as dead</a>
    </li>
    <li>
        <a class="modal-trigger" href="#modalRehome" onclick="calculateCountries()">Mark as rehomed</a>
    </li>
</ul>

<script>
    const calculateCountries = () => {
        fetch('https://restcountries.eu/rest/v2/all?fields=name;flag;alpha2Code')
        .then(response => response.json())
        .then(x  => {
            var dataCountry = {};
            x.forEach(country => {
                dataCountry[country.name] = country.flag;
            });

            M.Autocomplete.init(document.querySelectorAll('#countryCode'), {
                data: dataCountry
            })
        })
    }
</script>

<!-- Rehome Modal -->
<div id="modalRehome" class="modal measurementModal">
    <div class="modal-content row">
        <form action="?animals" method="post" id="modalRehomeForm">
            <input type="hidden" name="modalRehomeAnimal" id="modalRehomeAnimal">
            <div class="col s6">
                <h4>Rehome animal</h4>
                <p>Animal referenceNumber: <span id="modalRehomeReference">Unknown</span></p>


                <div class="input-field">
                    <input id="newOwnerName" name="newOwnerName" type="text" maxlength="30">
                    <label for="newOwnerName">New Owner Name</label>
                </div>

                <div class="input-field">
                    <input id="newOwnerContactDetails" name="newOwnerContactDetails" type="text" maxlength="100">
                    <label for="newOwnerContactDetails">New Owner Contact Details</label>
                </div>
            </div>
            <div class="col s6">

                <div class="input-field">
                    <input id="existingConditions" name="existingConditions" type="text" maxlength="100">
                    <label for="existingConditions">Existing conditions</label>
                </div>

                <div class="input-field">
                    <input type="text" id="dateOfDeparture" name="dateOfDeparture" class="datepicker" placeholder="Leave empty for today">
                    <label for="dateOfDeparture">Date of departure</label>
                </div>

                <div class="input-field">
                    <input type="text" id="countryCode" name="countryCode" class="autocomplete">
                    <label for="countryCode">Receiving country</label>
                </div>

                <p>
                    <label>
                        <input type="checkbox" name="checklist">
                        <span>Rehoming checklist complete?</span>
                    </label>
                </p>

                <div class="s12">
                    <select required name="donation">
                        <option value="" disabled selected>Donation made?</option>

                        <option value="1"> Yes </option>
                        <option value="2"> No </option>
                        <option value="0"> Waved </option>
                    </select>
                </div>

                <select required name="personell">
                    <option value="" disabled selected>Choose Employee</option>

                    <?php
                        $personellCmd = $db->get("SELECT * FROM users");
                        while ($person = $personellCmd->fetch()):
                    ?>
                    <option value="<?= $person['username'] ?>"><?= $person['firstname'] ?> <?= $person['lastname'] ?> (<?= $person['username'] ?>) </option>
                    <?php endwhile; ?>
                </select>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a class="modal-close btn-flat left"><i class="material-icons left">cancel</i>Cancel</a>
        <a onclick="getElement('modalRehomeForm').submit()" class="btn-flat"><i class="material-icons left">clear_all</i>Rehome</a>
    </div>
</div>


<!-- Mortality Modal -->
<div id="modalMortality" class="modal measurementModal">
    <div class="modal-content row">
        <form action="?animals" method="post" id="modalMortalityForm">
            <input type="hidden" name="modalMortalityAnimal" id="modalMortalityAnimal">
            <div class="col s6">
                <h4>Mark animal as dead</h4>
                <p>Animal referenceNumber: <span id="modalMortalityReference">Unknown</span></p>

                <div class="col s12" style="margin-top: 20px;">
                    <p style="margin-bottom: 10px;">Notes / pm result</p>
                    <textarea name="notes" cols="30" rows="15" style="height:8em" required></textarea>
                </div>
            </div>
            <div class="col s6">

                <div class="input-field col s12">
                    <input type="text" id="dateOfDeath" name="dateOfDeath" class="datepicker" placeholder="Leave empty for today">
                    <label for="dateOfDeath">Date of death</label>
                </div>

                <div class="col s12">
                    <select name="deathMethod">
                        <option value="" disabled selected>Died naturally or euthanised?</option>
                        <option value="0">Died </option>
                        <optgroup label="Euthanised">
                            <option value="1">Medical</option>
                            <option value="2-chronic">Non-Medical</option>
                        </optgroup>
                    </select>
                </div>

                <div class="input-field col s12">
                    <input type="text" id="dateOfDeparture" name="dateOfDeparture" class="datepicker" placeholder="Leave empty for today">
                    <label for="dateOfDeparture">Date of departure</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="causeOfDeath" id="causeOfDeath" placeholder="Leave empty for unknown" maxlength="80">
                    <label for="causeOfDeath">Cause of death</label>
                </div>
                
                <div class="col s12">
                    <p>
                        <label>
                            <input type="checkbox" name="vetenerinaryCare">
                            <span>Under veterinary care</span>
                        </label>
                    </p>
                    <p>
                        <label>
                            <input type="checkbox" name="postMortem">
                            <span>Post-Mortem complete?</span>
                        </label>
                    </p>
                    <p>
                        <label>
                            <input type="checkbox" name="cadaverStored">
                            <span>Cadaver stored</span>
                        </label>
                    </p>
                </div>

                <div class="col s12">
                    <select required name="personell">
                        <option value="" disabled selected>Choose Employee</option>

                        <?php
                            $personellCmd = $db->get("SELECT * FROM users");
                            while ($person = $personellCmd->fetch()):
                        ?>
                        <option value="<?= $person['username'] ?>"><?= $person['firstname'] ?> <?= $person['lastname'] ?> (<?= $person['username'] ?>) </option>
                        <?php endwhile; ?>
                    </select>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a class="modal-close btn-flat left"><i class="material-icons left">cancel</i>Cancel</a>
        <a onclick="getElement('modalMortalityForm').submit()" class="btn-flat"><i class="material-icons left">clear_all</i>Mark as dead</a>
    </div>
</div>
        

<!-- Add Choice Modal -->
<div id="modalChoice" class="modal">
    <div class="modal-content">
        <h4>Add measurement</h4>
        <p>What kind of measurement, do you want to add?</p>
    </div>
    <div class="modal-footer">
        
        <a class="tooltipped btn modal-trigger modal-close" href="#modalWeight" data-tooltip="Add weight measurement">
            <span class="mdi mdi-weight"></span>
        </a>

        <a class="tooltipped btn modal-trigger modal-close" href="#modalLength" data-tooltip="Add length measurement">
            <span class="mdi mdi-tape-measure"></span>
        </a>
        
        <a class="tooltipped btn modal-trigger modal-close" href="#modalCleaned" data-tooltip="Mark as cleaned">
            <span class="mdi mdi-spray-bottle"></span>
        </a>
        
        <a class="tooltipped btn modal-trigger modal-close" href="#modalFed" data-tooltip="Mark as fed">
            <span class="mdi mdi-food-apple"></span>
        </a>

        <a class="tooltipped btn modal-trigger modal-close" href="#modalTreated" data-tooltip="Mark as treated">
            <span class="mdi mdi-needle"></span>
        </a>

    </div>
</div>
<!-- Add weight Modal -->
<div id="modalWeight" class="modal measurementModal">
    <div class="modal-content row">
        <form action="?animals" method="post" id="modalWeightForm">
            <input type="hidden" name="modalWeightAnimal" id="modalWeightAnimal">
            <div class="col s6">
                <h4>Add weight <span class="mdi mdi-weight" style="display: inline;font-size:1.1em"></span></h4>
                <p>Animal referenceNumber: <span id="modalWeightReference">Unknown</span></p>
            </div>
            <div class="col s6">
                <div class="input-field row">
                    <div class="col s10">
                        <label>
                            <input type="number" name="weight" required>
                        </label>
                    </div>
                    <div class="col s2">
                        <p>g</p>
                    </div>
                </div>
                <p>
                    <label>
                        <input type="checkbox" name="approximate">
                        <span>Is approximate?</span>
                    </label>
                </p>
                <input type="text" name="date" class="datepicker" placeholder="Leave empty for today">
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a class="modal-close btn-flat left"><i class="material-icons left">cancel</i>Cancel</a>
        <a onclick="getElement('modalWeightForm').submit()" class="btn-flat"><i class="material-icons left">clear_all</i>Add Measurement</a>
    </div>
</div>

<!-- Add length Modal -->
<div id="modalLength" class="modal measurementModal">
    <div class="modal-content row">
        <form action="?animals" method="post" id="modalLengthForm">
            <input type="hidden" name="modalLengthAnimal" id="modalLengthAnimal">
            <div class="col s6">
                <h4>Add length <span class="mdi mdi-tape-measure" style="display: inline;font-size:1.1em"></span></h4>
                <p>Animal referenceNumber: <span id="modalLengthReference">Unknown</span></p>
            </div>
            <div class="col s6">
                <div class="input-field row">
                    <div class="col s10">
                        <label>
                            <input type="number" name="length" required>
                        </label>
                    </div>
                    <div class="col s2">
                        <p>cm</p>
                    </div>
                </div>
                <p>
                    <label>
                        <input type="checkbox" name="approximate">
                        <span>Is approximate?</span>
                    </label>
                </p>
                <input type="text" name="date" class="datepicker" placeholder="Leave empty for today">
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a class="modal-close btn-flat left"><i class="material-icons left">cancel</i>Cancel</a>
        <a onclick="getElement('modalLengthForm').submit()" class="btn-flat"><i class="material-icons left">clear_all</i>Add Measurement</a>
    </div>
</div>

<!-- Mark Cleaned Modal -->
<div id="modalCleaned" class="modal measurementModal">
    <div class="modal-content row">
        <form action="?animals" method="post" id="modalCleanedForm">
            <input type="hidden" name="modalCleanedAnimal" id="modalCleanedAnimal">
            <div class="col s6">
                <h4>Mark cleaned <span class="mdi mdi-spray-bottle" style="display: inline;font-size:1.1em"></span></h4>
                <p>Animal referenceNumber: <span id="modalCleanedReference">Unknown</span></p>
            </div>
            <div class="col s6">
                <input type="text" name="date" class="datepicker" placeholder="Leave empty for today">
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a class="modal-close btn-flat left"><i class="material-icons left">cancel</i>Cancel</a>
        <a onclick="getElement('modalCleanedForm').submit()" class="btn-flat"><i class="material-icons left">clear_all</i>Add Measurement</a>
    </div>
</div>

<!-- Mark Fed Modal -->
<div id="modalFed" class="modal measurementModal">
    <div class="modal-content row">
        <form action="?animals" method="post" id="modalFedForm">
            <input type="hidden" name="modalFedAnimal" id="modalFedAnimal">
            <div class="col s6">
                <h4>Mark fed <span class="mdi mdi-food-apple" style="display: inline;font-size:1.1em"></span></h4>
                <p>Animal referenceNumber: <span id="modalFedReference">Unknown</span></p>
            </div>
            <div class="col s6">
                <input type="text" name="date" class="datepicker" placeholder="Leave empty for today">
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a class="modal-close btn-flat left"><i class="material-icons left">cancel</i>Cancel</a>
        <a onclick="getElement('modalFedForm').submit()" class="btn-flat"><i class="material-icons left">clear_all</i>Add Measurement</a>
    </div>
</div>

<!-- Mark Treated Modal -->
<div id="modalTreated" class="modal measurementModal">
    <div class="modal-content row">
        <form action="?animals" method="post" id="modalTreatedForm">
            <input type="hidden" name="modalTreatedAnimal" id="modalTreatedAnimal">
            <div class="col s6">
                <h4>Mark Treated <span class="mdi mdi-food-apple" style="display: inline;font-size:1.1em"></span></h4>
                <p>Animal referenceNumber: <span id="modalTreatedReference">Unknown</span></p>
            </div>
            <div class="col s6">
                <input type="text" name="date" class="datepicker" placeholder="Leave empty for today">
            </div>
            <div class="input-field col s6">
                <input type="text" name="notes" id="modalTreatedNotes" maxlength="100">
                <label for="modalTreatedNotes">Notes</label>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a class="modal-close btn-flat left"><i class="material-icons left">cancel</i>Cancel</a>
        <a onclick="getElement('modalTreatedForm').submit()" class="btn-flat"><i class="material-icons left">clear_all</i>Add Measurement</a>
    </div>
</div>
