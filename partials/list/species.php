<?php
    include 'partials/view/_common.php';
    include 'partials/functions/citesToString.php';

    if (isset($_GET['delete'])) {
        if ($db->getColumn("SELECT COUNT(*) FROM Animal WHERE species=?", [$_GET['delete']]) == 0) {
            $db->set("DELETE FROM Species WHERE speciesId=?", [$_GET['delete']]);
            $db->log("Deleted species '". [$_GET['delete']] . "'", 1);
            echo "<script>history.pushState({}, '', '?species');</script>";
            echo "<script>document.addEventListener('DOMContentLoaded', function() {M.toast({html: 'Deleted Species!'});});</script>";
        } else {
            echo "<script>history.pushState({}, '', '?species');</script>";
            echo "<script>document.addEventListener('DOMContentLoaded', function() {M.toast({html: 'Could not delete Species, there are Animals of the species!'});});</script>";
        }
    }

    $readOnly = $db->getColumn("SELECT is_administrator FROM users WHERE username=?", [$_SESSION['username']]) == 2;
?>

<style>
    .hidden {
        display: none !important;
    }
</style>
<script>
    window.addEventListener("DOMContentLoaded", async () => {
        const searchBox = document.querySelector('#speciesSearch');
        searchBox.addEventListener('input', () => {
            if (currentlyCollapsed == true) toggleCollapseAll()
            document.querySelectorAll(`tr[class*="category"]`).forEach(element => {

                let found = false;
                [...element.children].forEach(child => {
                    if (child.innerText.toLowerCase().includes(searchBox.value.toLowerCase())) found = true;
                });

                if (found)
                    element.classList.remove('hidden')
                else
                    element.classList.add('hidden')
            })
        })

    })
</script>

<div class="row">

    <div class="col s12">
        <div class="right">
            <br>

            <div style="display: grid;grid-template-columns:auto auto;gap:30px">
                <input type="text" id="speciesSearch" size="15">

                <?php if (!$readOnly): ?>
                <a class="btn tooltipped" data-position="bottom"
                    data-tooltip="Add new Owner Relinquishment"
                    href="/?page=edit&species">
                    <i class="material-icons left">add</i>Species
                </a>
                <?php endif; ?>
            </div>
        
        </div>
        <h2>Species</h2><br>
    </div>

    <div class="col s12">
        <table class="striped">
            <tr>
                <th>Category</th>
                <th>Species Name</th>
                <th>Common Name</th>
                <th>Cites</th>
                <th>DWAA</th>
                <th>IAS</th>
                <th>Is native</th>
                <th></th>
            </tr>

            <?php
                $cmd = $db->get("SELECT category FROM Species GROUP BY category");
                while ($row = $cmd->fetch()):
            ?>
                <tr onclick="toggleCollapse(<?= $row['category'] ?>)" class="click">
                    <th colspan="7"><?= categoryToString($row['category']) ?></th>
                </tr>

                <?php
                    $speciesCmd = $db->get("SELECT * FROM Species WHERE category=? ORDER BY speciesName", [$row['category']]);
                    while ($species = $speciesCmd->fetch()):
                ?>
                    <tr class="category<?= $row['category'] ?>">
                        <td><?= categoryToString($species['category']) ?></td>
                        <td><?= $species['speciesName'] ?></td>
                        <td><?= $species['speciesCommonName'] ?></td>
                        <td><?= citesToString($species['cites']) ?></td>
                        <td><?= boolToIcon($species['dwaa']) ?></td>
                        <td><?= boolToIcon($species['ias']) ?></td>
                        <td><?= boolToIcon($species['isNative']) ?></td>
                        <td class="right">
                            <!-- This has been disabled, because there is currently no way of dealing with issues regarding this species -->
                            <a class="tooltipped btn modal-trigger thirdParty" href="#modalDelete" data-tooltip="Remove species"
                            onclick="getElement('modalDeleteButton').href = '?species&delete=' + <?= $species['speciesId'] ?>">
                                <span class="mdi mdi-delete"></span>
                            </a>
                            
                            <!-- <a class="tooltipped btn modal-trigger thirdParty" data-tooltip="Edit">
                                <span class="mdi mdi-square-edit-outline"></span>
                            </a> -->
                        </td>
                    </tr>

                <?php endwhile; ?>
            <?php endwhile; ?>
            
        </table>
    </div>

</div>



<!-- Mark as Book Out Modal -->
<div id="modalDelete" class="modal">
    <div class="modal-content">
        <h4>Remove species</h4>
        <p>Do you really want to delete the species?</p>
    </div>
    <div class="modal-footer">
        <a class="modal-close btn-flat left"><i class="material-icons left">cancel</i>Cancel</a>
        <a id="modalDeleteButton" href="" class="btn"><i class="material-icons left">exit_to_app</i>Delete Species</a>
    </div>
</div>


<style>
    .click { border-bottom: 1px solid rgba(0, 0, 0, 0.12) !important }
    table.striped > tbody > tr:nth-child(2n+1) { background-color:unset }
</style>

<?php $db->log("Listing species in database"); ?>