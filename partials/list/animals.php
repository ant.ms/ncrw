<?php
    include 'partials/view/_common.php';
    include "partials/list/animals_logic.php";
?>

<script>
    updateModalWithReferenceNumber = async (referenceNumber) => {
        await new Promise(r => setTimeout(r, 25));

        getElement('modalMortalityAnimal').value = referenceNumber;
        getElement('modalMortalityReference').href = referenceNumber;
        
        getElement('modalRehomeAnimal').value = referenceNumber;
        getElement('modalRehomeReference').innerText = referenceNumber;
    }

    fillDropdowns = (referenceNumber) => {
        getElement('modalWeightAnimal').value = referenceNumber;
        getElement('modalWeightReference').innerText = referenceNumber;

        getElement('modalLengthAnimal').value = referenceNumber;
        getElement('modalLengthReference').innerText = referenceNumber;

        getElement('modalCleanedAnimal').value = referenceNumber;
        getElement('modalCleanedReference').innerText = referenceNumber;

        getElement('modalFedAnimal').value = referenceNumber;
        getElement('modalFedReference').innerText = referenceNumber;

        getElement('modalTreatedAnimal').value = referenceNumber;
        getElement('modalTreatedReference').innerText = referenceNumber;
    }
</script>
<style>
    .hidden {
        display: none !important;
    }
</style>
<script>
    window.addEventListener("DOMContentLoaded", async () => {

        const searchBox = document.querySelector('#animalSearch');
        searchBox.addEventListener('input', () => {
            if (currentlyCollapsed == true) toggleCollapseAll()

            document.querySelectorAll(`tr[class*="category"]`).forEach(element => {
                let found = false;
                [...element.children].forEach(child => {
                    if (child.innerText.toLowerCase().includes(searchBox.value.toLowerCase())) found = true;
                });

                if (found)
                    element.classList.remove('hidden')
                else
                    element.classList.add('hidden')
            })

        })

    })
</script>

<div class="row">

    <div class="col s12">
        <div class="right">
            <br>

            <div style="display: grid;grid-auto-flow: column;align-items:center">
                <input type="text" id="animalSearch" size="15">

                <label class="tooltipped" data-position="bottom" data-tooltip="Show hidden">
                    <input type="checkbox" <?= (isset($_GET['hidden']) ? "checked" : "") ?> onclick="location.href = '?animals<?= (isset($_GET['hidden']) ? "" : "&hidden") ?>'"><span>​</span>
                </label>
                
                <?php if (!$readOnly): ?>
                <a class="btn" href="/?page=edit&animal">
                    <i class="material-icons left">add</i>Animal
                </a>
                <?php endif; ?>
            </div>
        </div>
        <h2>Animals</h2><br>
    </div>

    <div class="col s12">
        <table class="striped">
            <tr>
                <th>NCRW reference number</th>
                <th>Name</th>
                <th>Sex</th>
                <th>Species</th>
                <th></th>
            </tr>

            <?php
                $categoryCmd = $db->get("SELECT category FROM Species GROUP BY category");
                while ($category = $categoryCmd->fetch()):
            ?>
                <tr onclick="toggleCollapse(<?= $category['category'] ?>)" class="click">
                    <th colspan="7"><?= categoryToString($category['category']) ?></th>
                </tr>

                <?php
                    $cmd = $db->getAnimalsFromCategory($category["category"], isset($_GET['hidden']));
                    while ($row = $cmd->fetch()):
                        $thirdParty = $row['isThirdParty'] == 1 ? "thirdParty" : "";
                ?>
                <tr class="category<?= $category['category'] ?>">
                    <td><?= $row['ncrwRefrenceNumber'] ?></td>
                    <td><?= $row['animalName'] ?></td>
                    <td><?= sexToIconString($row['sex']) ?></td>
                    <td><?= $db->GetColumn("SELECT speciesCommonName FROM Species WHERE speciesId = ?", [$row['species']]) ?> (<?= $db->GetColumn("SELECT speciesName FROM Species WHERE speciesId = ?", [$row['species']]) ?>)</td>
                    <td class="right">
                        <?php if (isset($_GET['hidden'])): ?>
                            <a class="tooltipped btn" href="?animals&revise=<?= $row['ncrwRefrenceNumber'] ?>" data-tooltip="Mark as un-dead and un-rehomed">
                                <span class="mdi mdi-delete-restore"></span>
                            </a>
                        <?php else: ?>
                            <?php if (!$readOnly): ?>
                            <a class="tooltipped btn modal-trigger <?= $thirdParty ?>" href="#modalChoice" data-tooltip="Add measurement"
                            onclick="fillDropdowns('<?= $row['ncrwRefrenceNumber'] ?>')">
                                <span class="mdi mdi-chevron-down"></span>
                            </a>
                            
                            <a style="margin-left: 10px;" class="tooltipped btn dropdown-trigger dropdownBookOutTrigger <?= $thirdParty ?>" data-target='dropdownBookOut' data-tooltip="Remove entry"
                                onclick="updateModalWithReferenceNumber('<?= $row['ncrwRefrenceNumber'] ?>')">
                                <span class="mdi mdi-delete"></span>
                            </a>
                            <?php endif; ?>

                            <a class="tooltipped btn modal-trigger <?= $thirdParty ?>" data-tooltip="Show QR code" href="card.php?animal=<?= $row['ncrwRefrenceNumber'] ?>">
                                <span class="mdi mdi-qrcode"></span>
                            </a>
                            <a class="tooltipped btn modal-trigger <?= $thirdParty ?>" 
                            data-tooltip="Display" href=".?view=<?= $row['ncrwRefrenceNumber'] ?>">
                                <span class="mdi mdi-eye"></span>
                            </a>
                            <!-- <a class="tooltipped btn modal-trigger <?= $thirdParty ?>" data-tooltip="Edit">
                                <span class="mdi mdi-square-edit-outline"></span>
                            </a> -->

                        <?php endif; ?>
                    </td>
                </tr>
                <?php endwhile; ?>
            <?php endwhile; ?>
            
        </table>
    </div>

</div>



<?php if (!$readOnly) include "partials/list/animals_modals.php"; ?>

<style>
    .thirdParty { background: var(--accent-light) !important }
    #modalChoice { width: 290px }
    .measurementModal > div.modal-content { min-height: 350px !important }
    .datepicker-modal { min-width: 500px }
    .click { border-bottom: 1px solid rgba(0, 0, 0, 0.12) !important }
    table.striped > tbody > tr:nth-child(2n+1) { background-color:unset }
</style>

<?php $db->log("Listing" . (isset($_GET['hidden']) ? " hidden " : " ") . "animals in database"); ?>