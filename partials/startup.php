<?php
require 'partials/database.php';
$db = new Database;

// check if installed (check if at least one user is entered)
if (!$db->getColumn("SELECT count(*) FROM users")) {
	include 'partials/install.php';
	exit;
}

session_start();

// check if logged in
if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
	include 'partials/login.php';
	exit;
}
?>