<?php
    class Database {
        private $pdo;

        // Enter the login details for your database here
        private $hostname = "127.0.0.1";
        private $dbname = "NCRW";
        private $username = "NCRWApp";
        private $password = "changeMe";

        public function __construct() {
            $this->pdo = new PDO("mysql:host=$this->hostname;dbname=$this->dbname", $this->username, $this->password);
        }

        public function _query($input) {
            $whereString = isset($input['check']) ? "WHERE" : "";

            // Selector
            $selector = is_array($input['selector'])
                        ? implode(", ", $input['selector'])
                        : $input['selector'];
            // Joins
            $joinString = "";
            if (in_array("species",     $input['joins']))
                $joinString = $joinString . " LEFT JOIN Species ON Species.speciesId=species ";
            if (in_array("rehoming",    $input['joins']))
                $joinString = $joinString . " LEFT JOIN rehoming ON ncrwRefrenceNumber=rehoming.referenceNumber ";
            if (in_array("mortality",  $input['joins']))
                $joinString = $joinString . " LEFT JOIN mortality ON ncrwRefrenceNumber=mortality.referenceNumber ";

            // Predefined check for animals gone (dead or out away)
            if (isset($input['hiddenCheck'])) {
                $hiddenNOT = $input['hiddenCheck'] == "hidden" ? "IS NOT" : "IS";
                $hiddenOR = $input['hiddenCheck']  == "hidden" ? "OR" : "AND";
                $whereString = $whereString . " rehoming.referenceNumber $hiddenNOT NULL $hiddenOR mortality.referenceNumber $hiddenNOT NULL ";
            }

            // Assemble Query
            $query = "SELECT $selector FROM ".$input['table'] . $joinString  . " " . $whereString . " ";
            if ($input['hiddenCheck'] && isset($input['check']) && $input['check'] != "")
                $query = $query . " AND ";
            if (isset($input['check']))
                $query = $query . " " . $input['check'];
            if (isset($input['group']))
                $query = $query . " GROUP BY " . $input['group'];

            // echo $query . "<br><br>";
            $cmd = $this->pdo->prepare($query);

            if (isset($input['args']))
                $cmd->execute($input['args']);
            else
                $cmd->execute();

            return $cmd;
        }

        public function query($input) {
            return $this->_query($input)->fetchAll();
        }

        // get a column from an sql command
        public function getColumn(string $query, array $args = null) {
            $cmd = $this->pdo->prepare($query);

            if (isset($args))
                $cmd->execute($args);
            else
                $cmd->execute();
            
            return $cmd->fetchColumn();
        }

        // get a column from the current user
        public function getUserColumn(string $column) {
            $cmd = $this->pdo->prepare("SELECT " . $column . " FROM users WHERE username=?");

            $cmd->execute([$_SESSION['username']]);
            
            return $cmd->fetchColumn();
        }

        // execute a given sql-command
        public function set(string $sql, array $args = null) {
            $cmd = $this->pdo->prepare($sql);

            if (isset($args))
                $cmd->execute($args);
            else
                $cmd->execute();
        }

        // execute values into a Table
        public function insert(string $table, array $columns = null) {
            // Get correctly separated list of keys
            $commaArray = implode(", ", array_keys($columns));
            $colonArray = ":" . implode(", :", array_keys($columns));

            $cmd = $this->pdo->prepare("INSERT INTO $table ($commaArray) VALUES ($colonArray)");

            $cmd->execute($columns);
        }

        public function insertMeasurement(string $animal, string $table, array $data) {
            $this->insert($table, $data + [
                'referenceNumber' => $animal,
                'dateRecorded' => ($_POST['date'] != "" ? $_POST['date'] : date(DATE_ATOM))
            ]);

            $name = preg_replace( '/([a-z0-9])([A-Z])/', "$1 $2", $table);
            $this->log("Added $name for animal: '$animal'", 1);
            echo "<script>document.addEventListener('DOMContentLoaded', function() {M.toast({html: 'Added $name for animal \'$animal\''});});</script>";
        }

        // execute a given query
        public function get(string $query, array $args = null) {
            $cmd = $this->pdo->prepare($query);

            if (isset($args))
                $cmd->execute($args);
            else
                $cmd->execute();
            
            return $cmd;
        }

        public function getAnimals(string $selector = "*", bool $hidden = false, string $additionalCheck = "") {
            $hiddenNOT = $hidden ? "IS NOT" : "IS";
            $hiddenOR = $hidden ? "OR" : "AND";
            $cmd = $this->get(implode(" " , [
                "SELECT $selector FROM Animal",
                "LEFT JOIN rehoming ON ncrwRefrenceNumber=rehoming.referenceNumber",
                "LEFT JOIN mortality ON ncrwRefrenceNumber=mortality.referenceNumber",
                "WHERE (rehoming.referenceNumber $hiddenNOT NULL $hiddenOR mortality.referenceNumber $hiddenNOT NULL) $additionalCheck ORDER BY animalName"
            ]));
            
            return $cmd;
        }

        public function getAnimalsFromCategory(int $categoryId, bool $hidden = false) {
            $hiddenNOT = $hidden ? "IS NOT" : "IS";
            $hiddenOR = $hidden ? "OR" : "AND";
            $cmd = $this->get(implode(" " , [
                "SELECT * FROM Animal",
                "LEFT JOIN Species ON Species.speciesId=species",
                "LEFT JOIN rehoming ON ncrwRefrenceNumber=rehoming.referenceNumber",
                "LEFT JOIN mortality ON ncrwRefrenceNumber=mortality.referenceNumber",
                "WHERE (rehoming.referenceNumber $hiddenNOT NULL $hiddenOR mortality.referenceNumber $hiddenNOT NULL) AND Species.category=$categoryId ORDER BY animalName"
            ]));
            
            return $cmd;
        }

        public function getAnimalsCustom(string $selector = "*", string $check) {
            $cmd = $this->get(implode(" " , [
                "SELECT $selector FROM Animal",
                "LEFT JOIN rehoming ON ncrwRefrenceNumber=rehoming.referenceNumber",
                "LEFT JOIN mortality ON ncrwRefrenceNumber=mortality.referenceNumber",
                "WHERE $check"
            ]));
            
            return $cmd;
        }

        public function getAnimalsSpecies(string $selector = "*", string $check) {
            $cmd = $this->get(implode(" " , [
                "SELECT $selector FROM Animal",
                "LEFT JOIN Species ON species=Species.speciesId",
                "LEFT JOIN rehoming ON ncrwRefrenceNumber=rehoming.referenceNumber",
                "LEFT JOIN mortality ON ncrwRefrenceNumber=mortality.referenceNumber",
                "WHERE $check"
            ]));
            
            return $cmd;
        }

        public function getAnimalsSpeciesDead(int $categoryId, string $additionalCheck = "") {
            $cmd = $this->get(implode(" " , [
                "SELECT COUNT(*) FROM mortality",
                "LEFT JOIN Animal ON mortality.referenceNumber=Animal.ncrwRefrenceNumber",
                "LEFT JOIN Species ON Species.speciesId=species",
                "WHERE Species.category=$categoryId $additionalCheck"
            ]));
            
            return $cmd;
        }
        public function getAnimalsSpeciesRehomed(int $categoryId, $additionalCheck = "*") {
            $cmd = $this->get(implode(" " , [
                "SELECT COUNT(*) FROM rehoming",
                "LEFT JOIN Animal ON rehoming.referenceNumber=Animal.ncrwRefrenceNumber",
                "LEFT JOIN Species ON Species.speciesId=species",
                "WHERE category=$categoryId $additionalCheck"
            ]));
            
            return $cmd;
        }

        public function log(string $content, int $importance = 0) {
            $cmd = $this->pdo->prepare("INSERT INTO logs VALUES (:dateLogged, :importance, :content, :username, :duration)");

            // importance:
            // 0 = info
            // 1 = log
            // 2 = error / something happened
            // 3 = critical

            $cmd->execute([
                'dateLogged' => date(DATE_ATOM),
                'importance' => $importance,
                'content' => $content,
                'duration' => microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"],
                'username' => $_SESSION['username']
            ]);
        }

        // return sql-dump string
        public function getDump() {            
            return shell_exec("mysqldump --user={$this->username} --password={$this->password} --host={$this->hostname} {$this->dbname}");
        }
    }
?>