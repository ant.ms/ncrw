<nav class="hide-on-large-only">
    <div class="nav-wrapper">
    <a href="#" class="brand-logo left hide-on-med-and-up">NCRW</a>
    <a href="#" class="brand-logo left hide-on-small-only">National Centre for Reptile Welfare (NCRW)</a>
        <ul id="nav-mobile" class="right">
            <li><a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a></li>
        </ul>
    </div>
</nav>