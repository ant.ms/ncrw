<script src="js/materialize.js"></script>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        M.FloatingActionButton.init(document.querySelectorAll('.fixed-action-btn'), {});
        M.Tooltip.init(document.querySelectorAll('.tooltipped'), {position: 'top'});
        M.Tabs.init(document.querySelectorAll('.tabs'), {})

        M.Dropdown.init(document.querySelectorAll('.dropdown-trigger'), { constrainWidth: false });
        M.Collapsible.init( document.querySelectorAll('.collapsible'), {});
        M.Sidenav.init(document.querySelectorAll('.sidenav'));
        M.Modal.init(document.querySelectorAll('.modal'), {});

        M.FormSelect.init(document.querySelectorAll('select'), {});
        M.Datepicker.init(document.querySelectorAll('.datepicker'), {
            format: "yyyy-mm-dd",
            maxDate: new Date(),
            yearRange: [new Date().getFullYear() - 100, new Date().getFullYear()]
        });
    });
</script>