<!DOCTYPE html>
<html>
    <head>
        <title>National Centre for Reptile Welfare (NCRW)</title>

        <?php include 'partials/head.php' ?>
        <link rel="stylesheet" href="css/_login.css">
    </head>

    <body>
    <div id="wrapper">
        <div style="height:25vh"></div>
            <div class="row">
                <div class="col s0 m2"></div>
                <div class="col s12 m8">
                    <div class="card install z-depth-5 center" style="max-width: 960px; margin: auto;">
                        <div class="card-content row">
                            <h2 style="margin-top: 5px">Install NCRW Database Software</h2>
                            <p>This function has been removed. If you see this page, please <b>immediately</b> message your manager or the person, that is currently maintaining this installation.</p>
                        </div>
                    </div>
                </div>
                <div class="col s0 m2"></div>
            </div>
        </div>

        <?php include 'partials/scripts.php' ?>
        <script src="js/utils.js"></script>
    </body>
</html>