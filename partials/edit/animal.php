<?php
    include 'partials/edit/animal_login.php';
?>


<script src="partials/edit/animalCheck.js"></script>
<form action="/?page=edit&animal" method="post" onsubmit="return checkAnimalForm()">

    <h2>Animal</h2><br>

    <div class="row" id="content">
        <div class="col hide-on-med-and-down l1"></div>

        <div class="col s12 m6 l4">

            <!-- #region Species -->
                <div class="row s12">
                    <div class="input-field row s12">
                        <!-- <i class="material-icons prefix">textsms</i> -->
                        <input type="text" id="autocomplete-input" class="autocomplete" name="species">
                        <label for="autocomplete-input">Search Species</label>
                    </div>
                    <p>
                        <label>
                            <input type="checkbox" name="article10">
                            <span>Has Article 10?</span>
                        </label>
                    </p>
                    <p>
                        <label>
                            <input type="checkbox" name="giftCert">
                            <span>Has gift certificate?</span>
                        </label>
                    </p>
                </div>

                <script>
                    document.addEventListener('DOMContentLoaded', function() {
                        var instances = M.Autocomplete.init(document.querySelectorAll('#autocomplete-input'), {
                            data: {
                                <?php
                                    $speciesCmd = $db->get("SELECT * FROM Species");
                                    while ($speciesRow = $speciesCmd->fetch()) {
                                        $speciesName = $speciesRow['speciesName'];
                                        $speciesCommonName = $speciesRow['speciesCommonName'];
                                        echo "\"$speciesName ($speciesCommonName)\": null,";
                                    }
                                ?>
                            }
                        });
                    });
                </script>
            <!-- #endregion -->

        

            <div class="row s12">
                <!-- <h6>Single animal / multiple animals</h6> -->
                <div class="col s3">
                    <p>Animal</p>
                </div>
                <div class="col s3">
                    <input type="number" name="animalCount1" min="1" value="1">
                </div>
                <div class="col s2">
                    <p>of</p>
                </div>
                <div class="col s3">
                    <input type="number" name="animalCount2" min="1" value="1">
                </div>
            </div>


            <div class="card">
                <div class="card-content">

                    <!-- This value gets updated, based on what tab is selected -->
                    <input type="hidden" name="isThirdParty" id="isThirdParty" value="yes">
                    <script>
                        const setThirdParty = state => document.getElementById('isThirdParty').value = state;
                    </script>

                    <div class="row">

                        <div class="col s12">
                            <ul class="tabs">
                                <li class="tab col s6"><a class="active" href="#thirdParty" onclick="setThirdParty('yes')">Third Party</a></li>
                                <li class="tab col s6"><a href="#ownerRelinquishment" onclick="setThirdParty('no')">Owner Relinquishment</a></li>
                            </ul>
                        </div>

                        <div id="ownerRelinquishment" class="col s12"><br>
                            <div class="col s12">
                                <h6>DurationOwned</h6>
                                <div class="col s10">
                                    <label>
                                        <input type="number" class="inside inOwner" name="durationOwned">
                                    </label>
                                </div>
                                <div class="col s2">
                                    <p>years</p>
                                </div>
                            </div>

                            <div class="col s12">
                                <h6>Reason for rehoming</h6>
                                <div class="col s9">
                                    <input type="text" class="inside inOwner" name="reasonForRehoming" id="r4r" maxlength="80">
                                </div>
                                <div class="col s3">
                                <a class='dropdown-trigger btn' data-target='reasonForRehomingDropdown'> <i class="material-icons">arrow_drop_down</i> </a>
                                </div>
                            </div>
                            <div class="row s12">

                            </div>
                            <ul id='reasonForRehomingDropdown' class='dropdown-content'>
                                <li><a onclick="getElement('r4r').value = this.innerText">Ill Health</a></li>
                                <li><a onclick="getElement('r4r').value = this.innerText">Moving Home</a></li>
                                <li><a onclick="getElement('r4r').value = this.innerText">Landlord Issues</a></li>
                                <li><a onclick="getElement('r4r').value = this.innerText">Cost of care</a></li>
                                <li><a onclick="getElement('r4r').value = this.innerText">Not able to provide appropriate facilities</a></li>
                                <li><a onclick="getElement('r4r').value = this.innerText">Animal Illness</a></li>
                                <li><a onclick="getElement('r4r').value = this.innerText">Animal Size</a></li>
                                <li><a onclick="getElement('r4r').value = this.innerText">Too many Animals</a></li>
                                <li><a onclick="getElement('r4r').value = this.innerText">Aggression</a></li>
                                <li><a onclick="getElement('r4r').value = this.innerText">Loss of interest</a></li>
                                <li><a onclick="getElement('r4r').value = this.innerText">Pregnancy</a></li>
                            </ul>
                        </div>


                        <div id="thirdParty" class="col s12"><br>

                            <div class="row s12">
                                <select name="origin" class="inside inThirdParty">
                                    <option value="" disabled selected>Choose Origin</option>

                                    <option value="captivity"> Captivity </option>
                                    <option value="found"> Found in the Wild </option>
                                    <option value="import"> Import </option>
                                </select>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
            <br>


            <div class="row s12">
                <h6>Donor</h6>
                <div style="margin-left: 10.5px">
                <select name="personDonor">
                    <option value="Unknown" selected>Unknown</option>
                    <option value="Vet">Vet</option>
                    <option value="RSPCA">RSPCA</option>
                    <option value="Rescue Centre">Rescue Centre</option>
                    <option value="Relative">Relative</option>
                    <option value="{ublic">Public</option>
                    <option value="Police">Police</option>
                    <option value="Pet Shop">Pet Shop</option>
                    <option value="PDSA">PDSA</option>
                    <option value="Owner">Owner</option>
                    <option value="NHS">NHS</option>
                    <option value="Local Authority">Local Authority</option>
                    <option value="Landlord">Landlord</option>
                    <option value="Institution">Institution</option>
                    <option value="Import">Import</option>
                    <option value="Housing Association">Housing Association</option>
                    <option value="Home Owner">Home Owner</option>
                    <option value="Border Force">Border Force</option>
                    <option value="Blue Cross">Blue Cross</option>
                </select>
                </div>
            </div>
            <div class="row s12">
                <h6>Name of Person (who brought the animal in)</h6>
                <div class="col s12">
                    <input type="text" name="personName" maxlength="80">
                </div>
            </div>
            <div class="row s12">
                <h6>Email</h6>
                <div class="col s12">
                    <input type="email" name="personEmail" maxlength="120">
                </div>
            </div>
            <div class="row s12">
                <h6>Phone</h6>
                <div class="col s12">
                    <input type="tel" name="personPhone" maxlength="120">
                </div>
            </div>

            <br>
            <br>
            <br>

            <div class="row s12">
                <h6>Morph</h6>
                <div class="col s12">
                    <input type="text" name="morph" maxlength="80">
                </div>
            </div>
            
            <div class="row s12">
                <h6>Microchip</h6>
                <div class="col s12">
                    <input type="text" name="microchip" maxlength="40" placeholder="Leave empty for none">
                </div>
            </div>

            <div class="row s12">
                <h6>Animal name</h6>
                <div class="col s12">
                    <input type="text" name="name" maxlength="20">
                </div>
            </div>
            
            <div class="row s12">
                <h6>Date of birth</h6>
                <div class="col s12">
                    <input type="text" name="dateOfBirth" placeholder="Leave empty for unknown" class="datepicker">
                </div>
            </div>

            <div class="row s12">
                <h6>Background / Veterinary history</h6>
                <div class="col s12">
                    <input type="text" name="background" maxlength="200">
                </div>
            </div>
            
            <div class="row s12">
                <h6>Country</h6>
                <div class="col s12">
                <select name="country">
                    <option value="AF">Afghanistan</option>
                    <option value="AX">Åland Islands</option>
                    <option value="AL">Albania</option>
                    <option value="DZ">Algeria</option>
                    <option value="AS">American Samoa</option>
                    <option value="AD">Andorra</option>
                    <option value="AO">Angola</option>
                    <option value="AI">Anguilla</option>
                    <option value="AQ">Antarctica</option>
                    <option value="AG">Antigua and Barbuda</option>
                    <option value="AR">Argentina</option>
                    <option value="AM">Armenia</option>
                    <option value="AW">Aruba</option>
                    <option value="AU">Australia</option>
                    <option value="AT">Austria</option>
                    <option value="AZ">Azerbaijan</option>
                    <option value="BS">Bahamas</option>
                    <option value="BH">Bahrain</option>
                    <option value="BD">Bangladesh</option>
                    <option value="BB">Barbados</option>
                    <option value="BY">Belarus</option>
                    <option value="BE">Belgium</option>
                    <option value="BZ">Belize</option>
                    <option value="BJ">Benin</option>
                    <option value="BM">Bermuda</option>
                    <option value="BT">Bhutan</option>
                    <option value="BO">Bolivia, Plurinational State of</option>
                    <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                    <option value="BA">Bosnia and Herzegovina</option>
                    <option value="BW">Botswana</option>
                    <option value="BV">Bouvet Island</option>
                    <option value="BR">Brazil</option>
                    <option value="IO">British Indian Ocean Territory</option>
                    <option value="BN">Brunei Darussalam</option>
                    <option value="BG">Bulgaria</option>
                    <option value="BF">Burkina Faso</option>
                    <option value="BI">Burundi</option>
                    <option value="KH">Cambodia</option>
                    <option value="CM">Cameroon</option>
                    <option value="CA">Canada</option>
                    <option value="CV">Cape Verde</option>
                    <option value="KY">Cayman Islands</option>
                    <option value="CF">Central African Republic</option>
                    <option value="TD">Chad</option>
                    <option value="CL">Chile</option>
                    <option value="CN">China</option>
                    <option value="CX">Christmas Island</option>
                    <option value="CC">Cocos (Keeling) Islands</option>
                    <option value="CO">Colombia</option>
                    <option value="KM">Comoros</option>
                    <option value="CG">Congo</option>
                    <option value="CD">Congo, the Democratic Republic of the</option>
                    <option value="CK">Cook Islands</option>
                    <option value="CR">Costa Rica</option>
                    <option value="CI">Côte d'Ivoire</option>
                    <option value="HR">Croatia</option>
                    <option value="CU">Cuba</option>
                    <option value="CW">Curaçao</option>
                    <option value="CY">Cyprus</option>
                    <option value="CZ">Czech Republic</option>
                    <option value="DK">Denmark</option>
                    <option value="DJ">Djibouti</option>
                    <option value="DM">Dominica</option>
                    <option value="DO">Dominican Republic</option>
                    <option value="EC">Ecuador</option>
                    <option value="EG">Egypt</option>
                    <option value="SV">El Salvador</option>
                    <option value="GQ">Equatorial Guinea</option>
                    <option value="ER">Eritrea</option>
                    <option value="EE">Estonia</option>
                    <option value="ET">Ethiopia</option>
                    <option value="FK">Falkland Islands (Malvinas)</option>
                    <option value="FO">Faroe Islands</option>
                    <option value="FJ">Fiji</option>
                    <option value="FI">Finland</option>
                    <option value="FR">France</option>
                    <option value="GF">French Guiana</option>
                    <option value="PF">French Polynesia</option>
                    <option value="TF">French Southern Territories</option>
                    <option value="GA">Gabon</option>
                    <option value="GM">Gambia</option>
                    <option value="GE">Georgia</option>
                    <option value="DE">Germany</option>
                    <option value="GH">Ghana</option>
                    <option value="GI">Gibraltar</option>
                    <option value="GR">Greece</option>
                    <option value="GL">Greenland</option>
                    <option value="GD">Grenada</option>
                    <option value="GP">Guadeloupe</option>
                    <option value="GU">Guam</option>
                    <option value="GT">Guatemala</option>
                    <option value="GG">Guernsey</option>
                    <option value="GN">Guinea</option>
                    <option value="GW">Guinea-Bissau</option>
                    <option value="GY">Guyana</option>
                    <option value="HT">Haiti</option>
                    <option value="HM">Heard Island and McDonald Islands</option>
                    <option value="VA">Holy See (Vatican City State)</option>
                    <option value="HN">Honduras</option>
                    <option value="HK">Hong Kong</option>
                    <option value="HU">Hungary</option>
                    <option value="IS">Iceland</option>
                    <option value="IN">India</option>
                    <option value="ID">Indonesia</option>
                    <option value="IR">Iran, Islamic Republic of</option>
                    <option value="IQ">Iraq</option>
                    <option value="IE">Ireland</option>
                    <option value="IM">Isle of Man</option>
                    <option value="IL">Israel</option>
                    <option value="IT">Italy</option>
                    <option value="JM">Jamaica</option>
                    <option value="JP">Japan</option>
                    <option value="JE">Jersey</option>
                    <option value="JO">Jordan</option>
                    <option value="KZ">Kazakhstan</option>
                    <option value="KE">Kenya</option>
                    <option value="KI">Kiribati</option>
                    <option value="KP">Korea, Democratic People's Republic of</option>
                    <option value="KR">Korea, Republic of</option>
                    <option value="KW">Kuwait</option>
                    <option value="KG">Kyrgyzstan</option>
                    <option value="LA">Lao People's Democratic Republic</option>
                    <option value="LV">Latvia</option>
                    <option value="LB">Lebanon</option>
                    <option value="LS">Lesotho</option>
                    <option value="LR">Liberia</option>
                    <option value="LY">Libya</option>
                    <option value="LI">Liechtenstein</option>
                    <option value="LT">Lithuania</option>
                    <option value="LU">Luxembourg</option>
                    <option value="MO">Macao</option>
                    <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                    <option value="MG">Madagascar</option>
                    <option value="MW">Malawi</option>
                    <option value="MY">Malaysia</option>
                    <option value="MV">Maldives</option>
                    <option value="ML">Mali</option>
                    <option value="MT">Malta</option>
                    <option value="MH">Marshall Islands</option>
                    <option value="MQ">Martinique</option>
                    <option value="MR">Mauritania</option>
                    <option value="MU">Mauritius</option>
                    <option value="YT">Mayotte</option>
                    <option value="MX">Mexico</option>
                    <option value="FM">Micronesia, Federated States of</option>
                    <option value="MD">Moldova, Republic of</option>
                    <option value="MC">Monaco</option>
                    <option value="MN">Mongolia</option>
                    <option value="ME">Montenegro</option>
                    <option value="MS">Montserrat</option>
                    <option value="MA">Morocco</option>
                    <option value="MZ">Mozambique</option>
                    <option value="MM">Myanmar</option>
                    <option value="NA">Namibia</option>
                    <option value="NR">Nauru</option>
                    <option value="NP">Nepal</option>
                    <option value="NL">Netherlands</option>
                    <option value="NC">New Caledonia</option>
                    <option value="NZ">New Zealand</option>
                    <option value="NI">Nicaragua</option>
                    <option value="NE">Niger</option>
                    <option value="NG">Nigeria</option>
                    <option value="NU">Niue</option>
                    <option value="NF">Norfolk Island</option>
                    <option value="MP">Northern Mariana Islands</option>
                    <option value="NO">Norway</option>
                    <option value="OM">Oman</option>
                    <option value="PK">Pakistan</option>
                    <option value="PW">Palau</option>
                    <option value="PS">Palestinian Territory, Occupied</option>
                    <option value="PA">Panama</option>
                    <option value="PG">Papua New Guinea</option>
                    <option value="PY">Paraguay</option>
                    <option value="PE">Peru</option>
                    <option value="PH">Philippines</option>
                    <option value="PN">Pitcairn</option>
                    <option value="PL">Poland</option>
                    <option value="PT">Portugal</option>
                    <option value="PR">Puerto Rico</option>
                    <option value="QA">Qatar</option>
                    <option value="RE">Réunion</option>
                    <option value="RO">Romania</option>
                    <option value="RU">Russian Federation</option>
                    <option value="RW">Rwanda</option>
                    <option value="BL">Saint Barthélemy</option>
                    <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                    <option value="KN">Saint Kitts and Nevis</option>
                    <option value="LC">Saint Lucia</option>
                    <option value="MF">Saint Martin (French part)</option>
                    <option value="PM">Saint Pierre and Miquelon</option>
                    <option value="VC">Saint Vincent and the Grenadines</option>
                    <option value="WS">Samoa</option>
                    <option value="SM">San Marino</option>
                    <option value="ST">Sao Tome and Principe</option>
                    <option value="SA">Saudi Arabia</option>
                    <option value="SN">Senegal</option>
                    <option value="RS">Serbia</option>
                    <option value="SC">Seychelles</option>
                    <option value="SL">Sierra Leone</option>
                    <option value="SG">Singapore</option>
                    <option value="SX">Sint Maarten (Dutch part)</option>
                    <option value="SK">Slovakia</option>
                    <option value="SI">Slovenia</option>
                    <option value="SB">Solomon Islands</option>
                    <option value="SO">Somalia</option>
                    <option value="ZA">South Africa</option>
                    <option value="GS">South Georgia and the South Sandwich Islands</option>
                    <option value="SS">South Sudan</option>
                    <option value="ES">Spain</option>
                    <option value="LK">Sri Lanka</option>
                    <option value="SD">Sudan</option>
                    <option value="SR">Suriname</option>
                    <option value="SJ">Svalbard and Jan Mayen</option>
                    <option value="SZ">Swaziland</option>
                    <option value="SE">Sweden</option>
                    <option value="CH">Switzerland</option>
                    <option value="SY">Syrian Arab Republic</option>
                    <option value="TW">Taiwan, Province of China</option>
                    <option value="TJ">Tajikistan</option>
                    <option value="TZ">Tanzania, United Republic of</option>
                    <option value="TH">Thailand</option>
                    <option value="TL">Timor-Leste</option>
                    <option value="TG">Togo</option>
                    <option value="TK">Tokelau</option>
                    <option value="TO">Tonga</option>
                    <option value="TT">Trinidad and Tobago</option>
                    <option value="TN">Tunisia</option>
                    <option value="TR">Turkey</option>
                    <option value="TM">Turkmenistan</option>
                    <option value="TC">Turks and Caicos Islands</option>
                    <option value="TV">Tuvalu</option>
                    <option value="UG">Uganda</option>
                    <option value="UA">Ukraine</option>
                    <option value="AE">United Arab Emirates</option>
                    <option value="GB" selected>United Kingdom</option>
                    <option value="US">United States</option>
                    <option value="UM">United States Minor Outlying Islands</option>
                    <option value="UY">Uruguay</option>
                    <option value="UZ">Uzbekistan</option>
                    <option value="VU">Vanuatu</option>
                    <option value="VE">Venezuela, Bolivarian Republic of</option>
                    <option value="VN">Viet Nam</option>
                    <option value="VG">Virgin Islands, British</option>
                    <option value="VI">Virgin Islands, U.S.</option>
                    <option value="WF">Wallis and Futuna</option>
                    <option value="EH">Western Sahara</option>
                    <option value="YE">Yemen</option>
                    <option value="ZM">Zambia</option>
                    <option value="ZW">Zimbabwe</option>
                </select>
                </div>
            </div>

            <div class="row s12">
                <h6>Postcode</h6>
                <div class="col s12">
                    <input type="text" name="postcode" maxlength="10">
                </div>
            </div>

            <div class="row s12">
                <h6>How did they find us?</h6>
                <div class="col s9">
                    <input type="text" name="referral" id="referralInput" maxlength="40">
                </div>

                <div class="col s3">
                    <a class='dropdown-trigger btn' data-target='referralDropdown'> <i class="material-icons">arrow_drop_down</i> </a>
                </div>
            </div>
            <ul id='referralDropdown' class='dropdown-content'>
                <li><a onclick="getElement('referralInput').value = this.innerText">Vet</a></li>
                <li><a onclick="getElement('referralInput').value = this.innerText">Sspca</a></li>
                <li><a onclick="getElement('referralInput').value = this.innerText">Rspca</a></li>
                <li><a onclick="getElement('referralInput').value = this.innerText">Public</a></li>
                <li><a onclick="getElement('referralInput').value = this.innerText">Police</a></li>
                <li><a onclick="getElement('referralInput').value = this.innerText">Pet shop</a></li>
                <li><a onclick="getElement('referralInput').value = this.innerText">Pdsa</a></li>
                <li><a onclick="getElement('referralInput').value = this.innerText">Nhs</a></li>
                <li><a onclick="getElement('referralInput').value = this.innerText">Friends/family</a></li>
                <li><a onclick="getElement('referralInput').value = this.innerText">Know someone at the centre</a></li>
                <li><a onclick="getElement('referralInput').value = this.innerText">Internet</a></li>
                <li><a onclick="getElement('referralInput').value = this.innerText">Social media</a></li>
                <li><a onclick="getElement('referralInput').value = this.innerText">Breeder</a></li>
                <li><a onclick="getElement('referralInput').value = this.innerText">Commercial</a></li>
                <li><a onclick="getElement('referralInput').value = this.innerText">Import</a></li>
                <li><a onclick="getElement('referralInput').value = this.innerText">Local authority</a></li>
                <li><a onclick="getElement('referralInput').value = this.innerText">Rescue centre</a></li>
                <li><a onclick="getElement('referralInput').value = this.innerText">Show</a></li>
                <li><a onclick="getElement('referralInput').value = this.innerText">Unknown</a></li>
            </ul>

            <div class="row s12" style="margin-top: 20px;">
                <h6 style="margin-bottom: 10px;">Notes</h6>
                <div class="col s12">
                    <textarea name="notes" cols="30" rows="10" class="materialize-textarea"></textarea>
                </div>
            </div>

        </div>

        <div class="col hide-on-med-and-down l1"></div>

        <div class="col s12 m6 l4">

            <div class="row s12">
                <div class="row s12">
                    <select name="sex">
                        <option value="" disabled selected>Choose Sex</option>

                        <option value="1"> Male </option>
                        <option value="2"> Female </option>
                        <option value="0"> Unknown </option>
                    </select>
                </div>
                
                <div class="row s12">
                    <select name="source">
                        <option value="" disabled selected>Choose Source</option>

                        <option value="relatives"> Relatives </option>
                        <option value="public"> Public </option>
                        <option value="rehoming"> Rehoming </option>
                        <option value="localAuthority"> Local Authority </option>
                        <option value="police"> Police </option>
                        <option value="vets"> Vets </option>
                        <option value="commercial"> Commercial </option>
                        <option value="rehomingCentre"> Rehoming centre </option>
                        <option value="petShop"> Pet shop </option>
                        <option value="breeder"> Breeder </option>
                        <option value="show"> Show </option>
                        <option value="internet"> Internet </option>
                        <option value="unknown"> Unknown </option>
                    </select>
                </div>
                
                <div class="row s12">
                    <select name="room">
                        <option value="" disabled selected>Choose Room</option>

                        <option value="triage">Triage</option>
                        <option value="quarantine">Quarantine</option>
                        <option value="petShop">Pet shop</option>
                        <option value="mainRoom">Main room</option>
                        <option value="secondRehomingRoom">Second rehoming room</option>
                        <option value="turtleRoom">Turtle room</option>
                        <option value="venomousRoom">Venomous room</option>
                    </select>
                </div>
                
                <div class="row s12">
                    <select name="rack">
                        <option value="" disabled selected>Choose Rack</option>

                        <option value="rack">Rack 1 </option>
                        <option value="rack">Rack 2 </option>
                        <option value="rack">Rack 3 </option>
                        <option value="rack">Rack 4 </option>
                        <option value="rack">Rack 5 </option>
                        <option value="rack">Rack 6 </option>
                        <option value="rack">Rack 7 </option>
                        <option value="rack">Rack 8 </option>
                        <option value="rack">Rack 9 </option>
                        <option value="rack">Rack 10 </option>
                        <option value="rack">Main room left </option>
                        <option value="rack">Main room right </option>
                        <option value="rack">Second rehoming room </option>
                        <option value="rack">Turtle room </option>
                        <option value="rack">Pet shop </option>
                        <option value="rack">Venomous room </option>
                        <option value="rack">Dans home</option>
                    </select>
                </div>

            <div class="row s12">
                <select name="bodyCondition">
                    <option value="" disabled selected>Choose Body condition</option>

                    <option value="emanciated">Emaciated</option>
                    <option value="lean">Lean</option>
                    <option value="ideal">Ideal</option>
                    <option value="fat">Fat</option>
                    <option value="obese">Obese</option>
                </select>
            </div>

            <div class="row s12">
                <h6>Bite risk</h6>
                <p>
                    <label>
                        <input name="biterisk" type="radio" value="3">
                        <span>Red</span>
                    </label>
                </p>
                <p>
                    <label>
                        <input name="biterisk" type="radio" value="2">
                        <span>Orange</span>
                    </label>
                </p>
                <p>
                    <label>
                        <input name="biterisk" type="radio" value="1">
                        <span>Green</span>
                    </label>
                </p>
            </div>

            <div class="row s12">
                <h6>Confidence</h6>
                <p>
                    <label>
                        <input name="confidence" type="radio" value="3">
                        <span>High</span>
                    </label>
                </p>
                <p>
                    <label>
                        <input name="confidence" type="radio" value="2">
                        <span>Medium</span>
                    </label>
                </p>
                <p>
                    <label>
                        <input name="confidence" type="radio" value="1">
                        <span>Low</span>
                    </label>
                </p>
            </div>


            <div class="row s12">
                <select name="mbd">
                    <option value="no">No MBD</option>
                    <optgroup label="Chronic or not?">
                        <option value="chronic">Chronic MBD</option>
                        <option value="non-chronic">Non-Chronic MBD</option>
                    </optgroup>
                </select>
            </div>

            <p>
                <label>
                    <input type="checkbox" name="mites">
                    <span>Mites</span>
                </label>
            </p>

            <p>
                <label>
                    <input type="checkbox" name="euthanised">
                    <span>Euthanised</span>
                </label>
            </p>

            <br>

            <div class="row s12">
                <h6>Weight on entry</h6>
                <div class="col s10">
                    <label>
                        <input type="number" name="weight">
                    </label>
                </div>
                <div class="col s2">
                    <p>g</p>
                </div>
                <div class="col s12">
                    <p>
                        <label>
                            <input type="checkbox" name="weightApproximate">
                            <span>Is approximate?</span>
                        </label>
                    </p>
                </div>
            </div>

            <div class="row s12">
                <h6>Length on entry</h6>
                <div class="col s10">
                    <label>
                        <input type="number" name="length">
                    </label>
                </div>
                <div class="col s2">
                    <p>cm</p>
                </div>
                <div class="col s12">
                    <p>
                        <label>
                            <input type="checkbox" name="lengthApproximate">
                            <span>Is approximate?</span>
                        </label>
                    </p>
                </div>
            </div>

            <div class="row s12">
                <h6>Date of entry</h6>
                <div class="col s12">
                    <input type="text" name="dateOfEntry" placeholder="Leave empty for right now" class="datepicker">
                </div>
            </div>

            <p class="tooltipped" data-tooltip="This might change, if another person adds an entry at the same time">NCRW reference number: <?= getNewReferenceNumber() ?></p>

            <br>

            <button class="btn waves-effect waves-light" type="submit">Submit
                <i class="material-icons right">send</i>
            </button>

        </div>
    </div>

</form>