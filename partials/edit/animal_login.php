<?php
    include 'partials/functions/categoryToString.php';
    include 'partials/functions/getNewReferenceNumber.php';
    include 'partials/functions/getSpeciesIdFromName.php';

    // Add a new one
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $referenceNumber = getNewReferenceNumber();

        // handle third party exceptions
        $additionalRows = [ 'isThirdParty' => $_POST['isThirdParty'] == 'yes' ? 1 : 0 ];
        if ($_POST['isThirdParty'] == 'yes')
            $additionalRows = $additionalRows + [
                'origin' => $_POST['origin'],
            ];
        else
            $additionalRows = $additionalRows + [
                'durationOwned' => $_POST['durationOwned'],
                'reasonForRehoming' => $_POST['reasonForRehoming'],
            ];

        $db->insert("Animal", [
            'ncrwRefrenceNumber'    => $referenceNumber,
            'species'               => getSpeciesIdFromName($_POST['species']),

            'animalNumberCount1'    => $_POST['animalCount1'],
            'animalNumberCount2'    => $_POST['animalCount1'],
            
            'personDonor'            => $_POST['personDonor'],
            'personName'            => $_POST['personName'],
            'personEmail'         => $_POST['personEmail'],
            'personPhone'         => $_POST['personPhone'],
            'country'               => $_POST['country'],

            'room'                  => $_POST['room'],
            'rack'                  => $_POST['rack'],

            'morph'                 => $_POST['morph'],
            'animalName'            => $_POST['name'],
            'sex'                   => $_POST['sex'],
            'source'                => $_POST['source'],
            'background'            => $_POST['background'],
            'postcode'              => $_POST['postcode'],
            'dateOfEntry'           => ($_POST['dateOfEntry'] != "" ? $_POST['dateOfEntry'] : date("Y-m-d H:i:s")),
            'dateOfBirth'           => ($_POST['dateOfBirth'] != "" ? $_POST['dateOfBirth'] : "0000-00-00 00:00:00"),
            'referral'              => $_POST['referral'], // lcfirst(str_replace(' ', '', ucwords(strtolower($_POST['referral']))))
            'mites'                 => (isset($_POST['mites']) ? 1 : 0),
            'bodyCondition'         => $_POST['bodyCondition'],
            'mbd'                   => $_POST['mbd'],
            'microchip'             => $_POST['microchip'],
            'notes'                 => $_POST['notes'],
            'biterisk'              => $_POST['biterisk'],
            'confidence'            => $_POST['confidence'],
            'euthanised'            => (isset($_POST['euthanised']) ? 1 : 0),
            'has_article10'         => (isset($_POST['article10']) ? 1 : 0),
            'has_giftCert'          => (isset($_POST['giftCert']) ? 1 : 0),
            'createdBy'             => $_SESSION["username"]
        ] + $additionalRows);
        $db->log("Added animal '$referenceNumber'", 1);

        // WeightMeasurement
        $db->insert("WeightMeasurement", [
                'dateRecorded' => date(DATE_ATOM),
                'referenceNumber' => $referenceNumber,
                'isApproximate' => isset($_POST['weightApproximate']),
                'recordedWeight' => $_POST['weight']
            ]);
        $db->log("Added weight measurement for newly created animal ($referenceNumber)", 1);

        // LengthMeasurement
        $db->insert("LengthMeasurement", [
                'dateRecorded' => date(DATE_ATOM),
                'referenceNumber' => $referenceNumber,
                'isApproximate' => isset($_POST['lengthApproximate']),
                'recordedLength' => $_POST['length']
            ]);
        $db->log("Added length measurement for newly created animal ($referenceNumber)", 1);

    }
?>