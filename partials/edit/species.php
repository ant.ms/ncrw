<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        $category = $_POST['category'];
        if (is_numeric($category)) {
            $db->insert("Species", [
                'category' => $category,
                'speciesCommonName' => $_POST['speciesCommonName'],
                'speciesName' => $_POST['speciesName'],
                'cites' => $_POST['cites'],
                'dwaa' => (isset($_POST['dwaa']) ? 1 : 0),
                'ias' => (isset($_POST['ias']) ? 1 : 0),
                'isNative' => (isset($_POST['native']) ? 1 : 0),
            ]);
            $db->log("Added species '". $_POST['speciesName'] ."'", 1);
        }
        else {
            echo "<script>document.addEventListener('DOMContentLoaded', function() {M.toast({html: 'Something went wrong, please check your Species Category'});});</script>";
        }
    }
?>

<script src="js/checkForm.js"></script>
<form action="/?page=edit&species" method="post" id="mainForm">

    <h2>Add Species</h2><br>

    <div class="row" id="content">
        <div class="col hide-on-med-and-down l2"></div>

        <div class="col s12 l8">
            <div class="row s12">
                <select required name="category">
                    <option value="" disabled selected>Choose Category</option>

                    <option value="1">Snakes</option>
                    <option value="2">Lizards</option>
                    <option value="3">Tortoises</option>
                    <option value="4">Turtles</option>
                    <option value="5">Crocodilians</option>
                    <option value="6">Amphibians</option>
                    <option value="7">Invertebrates</option>
                </select>
            </div>

            <div class="row s12">
                <select name="cites">
                    <option value="no">No Annex</option>
                    <option value="A">Annex A</option>
                    <option value="B">Annex B</option>
                    <option value="C">Annex C</option>
                    <option value="D">Annex D</option>
                </select>
            </div>

            <div class="row s12">
                <h6>Species common name</h6>
                <div class="col s12">
                    <input type="text" name="speciesCommonName" maxlength="40" required>
                </div>
            </div>

            <div class="row s12">
                <h6>Species name</h6>
                <div class="col s12">
                    <input type="text" name="speciesName" maxlength="50" required>
                </div>
            </div>

            <p>
                <label>
                    <input type="checkbox" name="dwaa">
                    <span>DWAA</span>
                </label>
            </p>
            <p>
                <label>
                    <input type="checkbox" name="ias">
                    <span>IAS</span>
                </label>
            </p>
            <p>
                <label>
                    <input type="checkbox" name="native">
                    <span>Native</span>
                </label>
            </p>

            <button class="btn waves-effect waves-light" onclick="checkForm()">Submit
                <i class="material-icons right">send</i>
            </button>
        </div>
    </div>
</form>