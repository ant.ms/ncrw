const nameToString = input => {
    let result = input.replace( /([A-Z])/g, " $1" );
    return result.charAt(0).toUpperCase() + result.slice(1);
}
const checkType = (element, callback) => {
    let isThirdParty = document.getElementById('isThirdParty').value == "yes" ? true : false;

    if (element.classList.contains('inside')){
        if (element.classList.contains("inThirdParty") && isThirdParty)
            return callback(element)
        if (element.classList.contains("inOwner") && !isThirdParty)
            return callback(element)
        return true
    }
    else return callback(element)
}

const checkAnimalForm = () => {
    let statusArray = [];

    // Selects
    document.querySelectorAll('select').forEach(element => {
        statusArray.push(
            checkType( element, element => {
                if (element.value == "") {
                    M.toast({html: `Please '${element.innerText.split("\n")[1].trim()}'`});
                    return false;
                }
                return true;
            } )
        )
    });

    // Text
    document.querySelectorAll('input[type=text]:not([placeholder])').forEach(element => {
        statusArray.push(
            checkType( element, element => {
                if (element.value == "") {
                    M.toast({html: `Please fill out '${nameToString(element.name)}'`});
                    return false;
                }
                return true;
            } )
        )
    });

    // Number
    document.querySelectorAll('input[type=number]').forEach(element => {
        statusArray.push(
            checkType( element, element => {
                if (element.value == "") {
                    M.toast({html: `Please fill out '${nameToString(element.name)}'`});
                    return false;
                }
        
                if (element.value < 0) {
                    M.toast({html: `Please make sure '${nameToString(element.name)}' is a valid number`});
                    return false;
                }
                return true;
            } )
        )
    });

    return !statusArray.includes(false)
}

window.addEventListener("load", () => {
    const elems = document.querySelectorAll('input[type=text][maxlength]');
    elems.forEach(element => {
        element.setAttribute("data-length", element.getAttribute("maxlength"));
    })
    M.CharacterCounter.init(elems, {});
})