<?php
function getSpeciesIdFromName(string $name) {
    global $db;
    return $db->getColumn("SELECT speciesId FROM Species WHERE speciesName = ?", [ explode(" (", $name)[0] ]);
}
?>