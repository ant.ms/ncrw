<?php
function generateTable(string $table) {
    global $db;

    // Get columns
    $getColumns = $db->get("SHOW COLUMNS FROM $table;");
    $columns = [];
    while ($row = $getColumns->fetch()) {
        array_push($columns, [
            "field" => $row['Field'],
            "key" => isset($row['Key']) && $row['Key'] == "PRI"
        ]);
    }

    // Render Table
    echo "<div class='col s12'><h2>Raw Data: ".ucfirst($table)."</h2><br></div>";
    echo "<div class='col s12'><table class='striped'><tr>";
        foreach ($columns as $value) {
            echo "<th>";
            if ($value['key']) echo "<b>";
            echo $value['field'];
            if ($value['key']) echo "</b>";
            echo "</th>";
        }
    echo "</tr>";
    $cmd = $db->get("SELECT * FROM $table");
    while ($row = $cmd->fetch()){
        echo "<tr>";
        foreach ($columns as $key => $value) {
            $class = ($row[$key] == '0') ? "unimportant" : "";
            echo "<td class='$class'>".$row[$key]."</td>";
        }
        echo "</tr>";
    }
    echo "</table></div>";

    $db->log("Raw Data was accessed for Table: '$table'");
}
?>

<style>
    td, th {
        padding: 15px 40px;
    }
    .unimportant {
        filter: contrast(0.15);
    }
</style>