<?php
function boolToString(int $input) {
    return $input == 0 ? "no" : ($input == 1 ? "yes" : "_");
}
function boolToIcon(int $input) {
    return "<span class='mdi mdi-". ($input == 0 ? 'close' : 'check') ."'></span>";
}
?>