<?php
function sexToString(int $input) {
    if ($input == 1) return "m";
    if ($input == 2) return "f";
    return "?";
}

function sexToIconString(int $input) {
    if ($input == 1) return "<span class='mdi mdi-gender-male tooltipped' data-position='left' data-tooltip='Male'></span>";
    if ($input == 2) return "<span class='mdi mdi-gender-female tooltipped' data-position='left' data-tooltip='Female'></span>";
    return "?";
}

function sexToLongString(int $input) {
    if ($input == 1) return "Male";
    if ($input == 2) return "Female";
    return "Unknown";
}
?>