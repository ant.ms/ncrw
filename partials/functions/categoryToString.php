<?php
function categoryToString(int $input) {
    if ($input == 1) return "Snake";
    if ($input == 2) return "Lizards";
    if ($input == 3) return "Tortoises";
    if ($input == 4) return "Turtles";
    if ($input == 5) return "Crocodilians";
    if ($input == 6) return "Amphibians";
    if ($input == 7) return "Invertebrates";
    if ($input == 8) return "Frog";
    if ($input == 9) return "Salamander";
    if ($input == 10) return "Toad";
    if ($input == 11) return "Crustacean";
    if ($input == 12) return "Insect";
    if ($input == 13) return "Mollusc";
    if ($input == 14) return "Myriapod";
    if ($input == 15) return "Scorpion";
    if ($input == 16) return "Spider";
    if ($input == 17) return "Newt";
    return "Unknown";
}
?>
