<?php
function genMenuEntry(string $title, string $attribute) {
    $class = isset($_GET[$attribute]) ? "class='active'" : '';
    echo "<li $class><a href='?$attribute'>$title</a></li>";
}
function genSidebarTitle(string $title) {
    echo "<li><h4 class='sidebarTitle'>$title</h4></li>";
}
?>