<?php
function createUser(string $new_username, string $new_firstname, string $new_lastname, string $new_password, bool $isadmin = false) {
    global $db;

    // create connection to Database if not already connected
    if (!isset($db)) {
        include 'partials/database.php';
        $db = new Database;
    }

    // check input
    if (!preg_match('/^[A-Za-z0-9]+$/', $new_username)  || strlen($new_username) > 25  || strlen($new_username) < 5)  return false;
    if (!preg_match('/^[A-Za-z0-9]+$/', $new_firstname) || strlen($new_firstname) > 64 || strlen($new_firstname) < 3) return false;
    if (!preg_match('/^[A-Za-z0-9]+$/', $new_lastname)  || strlen($new_lastname) > 64  || strlen($new_lastname) < 3)  return false;

    // check if username is already taken
    if ($db->getColumn("SELECT COUNT(*) FROM users WHERE username=?", [$new_username]) != 0)
        return false;

    // Generate Login Hash
    $new_password_hash = hash('sha256', "This text has been salted and hashed" . $new_password . $new_username);

    // execute statement
    $db->set("INSERT INTO users (username, firstname, lastname, passwd, is_administrator, ui_theme, ui_accent, ui_grouping) VALUES (:username, :firstname, :lastname, :passwd, :isadmin, 'nord', 'blue', 1)", [
        'username' => $new_username,
        'firstname' => $new_firstname,
        'lastname' => $new_lastname,
        'passwd' => $new_password_hash,
        'isadmin' => (int)$isadmin
    ]);

    $db->log("Created user: '$new_username'", 2);
    return true;
}
?>