<?php
function toggleAdmin(string $username) {
    global $db;

    $currentStatus = $db->getColumn("SELECT is_administrator FROM users WHERE username=?", [$username]);

    $newStatus = -1;
         if ($currentStatus == "0") $newStatus = 1;
    else if ($currentStatus == "1") $newStatus = 2;
    else if ($currentStatus == "2") $newStatus = 0;

    $db->set('UPDATE users SET is_administrator=? WHERE username=?', [$newStatus, $username]);
    $db->log("Toggled administrator privileges to '".$newStatus."' for '" . $username . "'", 2);
}
?>