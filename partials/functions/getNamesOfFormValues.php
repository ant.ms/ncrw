<?php
function    getNamesOfFormValues(string $jsonFileName) {
    global $namesToProcess;

    $data = json_decode(file_get_contents($jsonFileName), true);

    foreach ($data as $group) {
        switch ($group['type']) {

            case 'select':
            case 'outOf':
            case 'ncrwReferenceNumber':
            case 'number':
            case 'textbox':
            case 'text': 
                array_push($namesToProcess, $group['dbName']);
                break;
    
            default: 
                foreach ($group['options'] as $value) {
                    array_push($namesToProcess, $group['dbName']);
    
                    // Textbox
                    if (isset($value['textboxIfSelected']))
                        array_push($namesToProcess, $value['textboxIfSelected']['dbName']);
                        
                    // Secondary Radio Buttons
                    if (isset($value['secondaryRadio']))
                        foreach ($value['secondaryRadio']['options'] as $secondaryValue) { 
                            array_push($namesToProcess, $value['secondaryRadio']['dbName']);

                            // Trinary Radio Buttons
                            if (isset($secondaryValue['trinaryRadio']))
                                foreach ($secondaryValue['trinaryRadio']['options'] as $trinaryValue)
                                    array_push($namesToProcess, $secondaryValue['trinaryRadio']['dbName']);
                        }
                }
        }
    }
}
?>