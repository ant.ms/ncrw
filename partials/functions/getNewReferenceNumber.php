<?php
function getNewReferenceNumber() {
    global $db;
    return date("y") . "-" . $db->getColumn("SELECT count(*) FROM Animal WHERE YEAR(dateOfEntry)=?", [date("Y")]);
}
?>