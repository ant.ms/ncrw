<tr style="background: var(--dark); position: sticky; top: 0">
    <th>Species Common Name</th>
    <th>Species Name</th>
    <th class="tooltipped" data-position="bottom" data-tooltip="Count of animals, that entered the NCRW through rehoming">Rehomed</th>
    <th class="tooltipped" data-position="bottom" data-tooltip="Count of animals, that entered the NCRW through being rescued">Rescued</th>
    <th class="tooltipped" data-position="bottom" data-tooltip="Count of animals, that died">Died</th>
    <th class="tooltipped" data-position="bottom" data-tooltip="Count of animals, that were euthanized">Euthanized</th>
    <th class="tooltipped" data-position="bottom" data-tooltip="Count of animals, that were rehomed (left NCRW)">Rehomed</th>
    <th class="tooltipped" data-position="bottom" data-tooltip="Is the species dwaa?">Dwaa</th>
    <th class="tooltipped" data-position="bottom" data-tooltip="Is the species ias?">Ias</th>
    <th class="tooltipped" data-position="bottom" data-tooltip="Is the species native?">Native</th>
</tr>