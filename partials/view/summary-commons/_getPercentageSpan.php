<?php
function getPercentageSpan($column, $value = false, $check = "category") {
    global $db;
    $countTrue = "";
    $countFalse = "";

    if ($value == false) {
        $data = $db->query([
            "selector" => [
                "IFNULL(SUM($column=1), 0) as yes",
                "IFNULL(SUM($column=0), 0) as no"
            ],
            "table" => "Animal",
            "check" => dateCheck(),
            "joins" => ["species", "rehoming", "mortality"]
        ])[0];
        $countTrue  = $data['yes'];
        $countFalse = $data['no'];
    } else {
        $data = $db->query([
            "selector" => [
                "IFNULL(SUM($column=1), 0) as yes",
                "IFNULL(SUM($column=0), 0) as no"
            ],
            "table" => "Animal",
            "check" => "$check=$value" . dateCheck(),
            "joins" => ["species", "rehoming", "mortality"]
        ])[0];
        $countTrue  = $data['yes'];
        $countFalse = $data['no'];
    }

    $percentage = round( $countTrue / ($countTrue + $countFalse) * 100 , 2);

    $class = ($countTrue == '0') ? "unimportant" : "";

    echo "<span class='tooltipped $class' data-position='top' data-tooltip='$percentage%'> <span>$countTrue</span> </span>";
}
?>