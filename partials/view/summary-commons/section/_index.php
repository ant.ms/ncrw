<?php
  $cmd = $db->get("SELECT category FROM Species GROUP BY category");
  while ($row = $cmd->fetch()):
?>

<tr onclick="toggleCollapse(<?= $row['category'] ?>)" class="click" style="background: var(--dark1); position: sticky; top: 54px">
    <th colspan="2"><?= categoryToString($row['category']) ?></th>
    <!-- Animals in -->
        <?php
        $inData = $db->query([
            "selector" => [
                "Count(*) as total",
                "IFNULL(SUM(isThirdParty=0), 0) as rehome",
                "IFNULL(SUM(isThirdParty=1), 0) as rescue"
            ],
            "table" => "Animal",
            "check" => "(rehoming.referenceNumber IS NULL AND mortality.referenceNumber IS NULL) AND Species.category=?" . dateCheck(),
            "args" => [$row['category']],
            "joins" => ["species", "rehoming", "mortality"],
            "hiddenCheck" => "shown"
        ])[0];
        ?>
        <th class='<?= ($inData['rehome'] == '0') ? "unimportant" : "" ?>'>
            <span><?= $inData['rehome'] ?></span>
        </th>
        <th class='<?= ($inData['rescue'] == '0') ? "unimportant" : "" ?>'>
            <span><?= $inData['rescue'] ?></span>
        </th>
        
    <!-- Animals dead -->
        <?php
        $count = $db->getAnimalsSpeciesDead($row['category'], "AND deathMethod=0".dateCheck('dateOfDeparture'))->fetchColumn();
        ?>
        <th class='<?= ($count == '0') ? "unimportant" : "" ?>'?>
            <span><?= $count ?></span>
        </th>

    <!-- Animals Euthanized -->
    <?php
        $count = $db->getAnimalsSpeciesDead($row['category'], "AND (deathMethod=1 OR deathMethod=2)".dateCheck('dateOfDeparture'))->fetchColumn();
        $countMedical = $db->getAnimalsSpeciesDead($row['category'], "AND deathMethod=1".dateCheck('dateOfDeparture'))->fetchColumn();
        $countNonMedical = $db->getAnimalsSpeciesDead($row['category'], "AND deathMethod=2".dateCheck('dateOfDeparture'))->fetchColumn();
    ?>
    <th class='<?= ($count == '0') ? "unimportant" : "" ?>'>
        <span class="tooltipped" data-position="top" data-tooltip="Medically: <?= $countMedical ?><br>Non-Medical: <?= $countNonMedical ?>">
            <?= $count ?>
        </span>
    </th>

    <!-- Animals rehomed -->
    <?php
        $count = $db->getAnimalsSpeciesRehomed($row['category'], dateCheck('dateOfDeparture'))->fetchColumn();
    ?>
    <th class='<?= ($count == '0') ? "unimportant" : "" ?>'>
        <span> <?= $count ?> </span>
    </th>

    <th><?= getPercentageSpan("dwaa", $row['category']) ?></th>
    <th><?= getPercentageSpan("ias", $row['category']) ?></th>
    <th><?= getPercentageSpan("isNative", $row['category']) ?></th>
</tr>

<?php include 'partials/view/summary-commons/section/content.php'; ?>

<?php endwhile; ?>