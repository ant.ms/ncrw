<?php    
    $speciesCmd = $db->get("SELECT * FROM Species WHERE category=?", [$row['category']]);
    while ($species = $speciesCmd->fetch()):

    $inData = $db->query([
        "selector" => "Count(*) as total, IFNULL(SUM(isThirdParty=0), 0) as rehome, IFNULL(SUM(isThirdParty=1), 0) as rescue",
        "table" => "Animal",
        "check" => "species=?" . dateCheck(),
        "args" => [$species['speciesId']],
        "joins" => ["species", "rehoming", "mortality"],
        "hiddenCheck" => "shown"
    ])[0];

    $deadData = $db->query([
        "selector" => [
            "IFNULL(SUM(deathMethod=0), 0) as naturally",
            "IFNULL(SUM(deathMethod=1 OR deathMethod=2), 0) as euthanized",
            "IFNULL(SUM(deathMethod=1), 0) as euthanizedMedical",
            "IFNULL(SUM(deathMethod=2), 0) as euthanizedNonMedical"
        ],
        "table" => "Animal",
        "check" => "mortality.referenceNumber IS NOT NULL AND species=?" . dateCheck(),
        "args" => [$species['speciesId']],
        "joins" => ["species", "rehoming", "mortality"]
    ])[0];
?>

<tr class="category<?= $row['category'] ?>">
    <td><?= $species['speciesCommonName'] ?></td>
    <td><?= $species['speciesName'] ?></td>
    <!-- Animals in -->
    <td class='<?= ($inData['rehome'] == '0') ? "unimportant" : "" ?>'>
        <span><?= $inData['rehome'] ?></span>
    </td>
    <td class='<?= ($inData['rescue'] == '0') ? "unimportant" : "" ?>'>
        <span><?= $inData['rescue'] ?></span>
    </td>
    
    <td class='<?= ($deadData['naturally'] == '0') ? "unimportant" : "" ?>'>
        <span><?= $deadData['naturally'] ?></span>
    </td>

    <td class='<?= ($deadData['euthanized'] == '0') ? "unimportant" : "" ?>'>
    <span class="tooltipped" data-position="top" data-tooltip="Medically: <?= $deadData['euthanizedMedical'] ?><br>Non-Medical: <?= $deadData['euthanizedNonMedical'] ?>">
        <span><?= $deadData['euthanized'] ?></span>
    </span>
    </td>
    
    <!-- Animals rehomed -->
    <?php
    $count = $db->getAnimalsCustom("Count(*)", "rehoming.referenceNumber IS NOT NULL AND species=". $species['speciesId'] . dateCheck())->fetchColumn();
    ?>
    <td class='<?= ($count == '0') ? "unimportant" : "" ?>'><span><?= $count ?></span></td>

    <td><?= getPercentageSpan("dwaa", $species['speciesId'], "species") ?></td>
    <td><?= getPercentageSpan("ias", $species['speciesId'], "species") ?></td>
    <td><?= getPercentageSpan("isNative", $species['speciesId'], "species") ?></td>
</tr>

<?php endwhile; ?>