<tr style="background: var(--dark);position: sticky; bottom: 0">
<th colspan="2">Total</th>
<!-- Animals Rehome and Rescue -->
  <?php
    $data = $db->query([
      "selector" => "Count(*) as total, IFNULL(SUM(isThirdParty=0), 0) as rehome, IFNULL(SUM(isThirdParty=1), 0) as rescue",
      "table" => "Animal",
      "check" => dateCheck(),
      "joins" => ["rehoming", "rehoming", "mortality"],
      "hiddenCheck" => "shown"
    ])[0];
  ?>
  <th class='<?= ($data['rehome'] == '0') ? "unimportant" : "" ?>'>
      <span><?= $data['rehome'] ?></span>
  </th>
  <th class='<?= ($data['rescue'] == '0') ? "unimportant" : "" ?>'>
  <span><?= $data['rescue'] ?></span>
  </th>

<!-- Died and Euthanized-->
  <?php
    $data = $db->query([
      "selector" => [
        "IFNULL(SUM(deathMethod=0), 0) as naturally",
        "IFNULL(SUM(deathMethod=1 OR deathMethod=2), 0) as euthanized",
        "IFNULL(SUM(deathMethod=1), 0) as euthanizedMedical",
        "IFNULL(SUM(deathMethod=2), 0) as euthanizedNonMedical"
      ],
      "table" => "Animal",
      "check" => dateCheck(),
      "joins" => ["rehoming", "rehoming", "mortality"],
      "hiddenCheck" => "hidden"
    ])[0];
  ?>
  <th class='<?= ($data['naturally'] == '0') ? "unimportant" : "" ?>'>
  <span><?= $data['naturally'] ?></span>
  </th>
  
  <th class='<?= ($data['euthanized'] == '0') ? "unimportant" : "" ?>'>
    <span class="tooltipped" data-position="top" data-tooltip="Medically: <?= $data['euthanizedMedical'] ?><br>Non-Medical: <?= $data['euthanizedNonMedical'] ?>">
    <span><?= $data['euthanized'] ?></span>
    </span>
  </th>

<!-- Rehomed -->
<th><?= $db->getAnimalsCustom("Count(*)", "rehoming.referenceNumber IS NOT NULL" . dateCheck())->fetchColumn() ?></th>

<th><?= getPercentageSpan("dwaa") ?>a</th>
<th><?= getPercentageSpan("ias") ?>a</th>
<th><?= getPercentageSpan("isNative") ?>a</th>
</tr>