<style>
  td span:first-child, th span:first-child {
    margin-right: -3px
  }
  td span:last-child, th span:last-child {
    margin-left: -3px
  }
  .unimportant > * {
    color: var(--light1) !important
  }
</style>

<?php include 'partials/view/summary-commons/_getPercentageSpan.php'; ?>

<table>
<?php include 'partials/view/summary-commons/header.php'; ?>

<?php include 'partials/view/summary-commons/section/_index.php'; ?>

<?php include 'partials/view/summary-commons/total.php'; ?>

</table>

<?php $db->log("Calculating summary view"); ?>