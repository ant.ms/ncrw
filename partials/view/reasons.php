<div class="row">
    <div class="col s12 l6">
        <h3>Rehoming reasons</h3>
        <table>
            <?php
                $cmd = $db->get("SELECT reasonForRehoming, count(*) as count FROM Animal WHERE isThirdParty=0 GROUP BY reasonForRehoming ORDER BY count DESC LIMIT 10");
                while ($row = $cmd->fetch()):
            ?>
            <tr>
                <td>
                    <?= $row['reasonForRehoming']  ? $row['reasonForRehoming'] : "Unknown" ?>
                    <a class="right btn">
                        <?= $row['count'] ?>
                    </a>
                </td>
            </tr>
            <?php endwhile; ?>
        </table>
    </div>
    <div class="col s12 l6">
        <h3>Rescue</h3>
        <table>
            <?php
                $cmd = $db->get("SELECT origin, count(*) as count FROM Animal WHERE isThirdParty=1 GROUP BY reasonForRehoming ORDER BY count DESC LIMIT 10");
                while ($row = $cmd->fetch()):
            ?>
            <tr>
                <td>
                    <?= $row['origin']  ? $row['origin'] : "Unknown" ?>
                    <a class="right btn">
                        <?= $row['count'] ?>
                    </a>
                </td>
            </tr>
            <?php endwhile; ?>
        </table>
    </div>
</div>

<?php $db->log("Accessed reasons table") ?>