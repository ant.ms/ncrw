<?php
    include 'partials/view/_common.php';
    $dateRange = isset($_GET['range']) ? $_GET['range'] : date("Y-m");
?>

<script>
    const switchDate = element => location.href = `?month&range=${element.value}`;
</script>

<div class="row">
    <div class="col s12">
        <div class="right" style="margin-right: 20px">
            <br>
            
            <select onchange="switchDate(this)">
                <?php
                    for ($i=0; $i < 12; $i++) {
                        $date = date('Y-m', strtotime("-$i month"));
                        $dateFormatted = date('F Y', strtotime("-$i month"));
                        $isSelected = ($date == $dateRange) ? "selected" : "";
                        echo "<option value='$date' $isSelected>$dateFormatted</option>";
                    }
                ?>
            </select>
        </div>

        <h2>Month Summary</h2><br>
    </div>

    <div class="col s12">

        <?php
            $year = ($dateRange == "*") ? "*" : substr($dateRange, 0, 4);
            $month = ($dateRange == "*") ? "*" : substr($dateRange, 5, 2);

            function dateCheck(string $column = "dateOfEntry") {
                global $dateRange, $month, $year;
                return ($dateRange == "*") ? "" : " AND MONTH($column)=$month AND YEAR($column)=$year";
            }
            
            include 'partials/view/summary-commons/_index.php';
        ?>

    </div>
</div>