<?php include 'partials/view/_common.php' ?>

<div class="col s12">
    <div class="right" style="margin-right: 30px">
        <a class="btn" onclick="toggleCollapseAll()"> <i class="material-icons">vertical_align_center</i> </a>
    </div>

    <h2>Animals in</h2><br>
</div>
<div class="col s12">
    <table>

        <tr style="background: var(--dark)">
            <th>Species Common Name</th>
            <th>Species Name</th>
            <th>Count active</th>
            <th>Count dead/rehomed</th>
            <th>CITES</th>
            <th>DWAA</th>
            <th>IAS</th>
            <th>Native</th>
        </tr>

        <?php
            $cmd = $db->get("SELECT category FROM Species GROUP BY category");
            while ($row = $cmd->fetch()):
        ?>

            <tr onclick="toggleCollapse(<?= $row['category'] ?>)" class="click">
                <th colspan="2"><?= categoryToString($row['category']) ?></th>
                <!-- Animals in -->
                <th><?= $db->getAnimalsSpecies("Count(*)", "(rehoming.referenceNumber IS NULL AND mortality.referenceNumber IS NULL) AND Species.category=".$row['category'] . $dateCheck)->fetchColumn() ?></th>
                <!-- Animals no longer in -->
                <th><?= $db->getAnimalsSpecies("Count(*)", "(rehoming.referenceNumber IS NOT NULL OR mortality.referenceNumber IS NOT NULL) AND Species.category=".$row['category'] . $dateCheck)->fetchColumn() ?></th>
                <th colspan="4"></th>
            </tr>
            <?php
                $speciesCmd = $db->get("SELECT * FROM Species WHERE category=?", [$row['category']]);
                while ($species = $speciesCmd->fetch()):
            ?>

                <tr class="category<?= $row['category'] ?>">
                    <td><?= $species['speciesCommonName'] ?></td>
                    <td><?= $species['speciesName'] ?></td>
                    <td><?= $db->getAnimals("Count(*)", false, "AND species=". $species['speciesId'])->fetchColumn() ?></td>
                    <td><?= $db->getAnimals("Count(*)", true, "AND species=". $species['speciesId'])->fetchColumn() ?></td>
                    <td>Annex <?= $species['cites'] ?></td>
                    <td><?= boolToIcon($species['dwaa']) ?></td>
                    <td><?= boolToIcon($species['ias']) ?></td>
                    <td><?= boolToIcon($species['isNative']) ?></td>
                </tr>

            <?php endwhile; ?>

            
        <?php endwhile; ?>

        <tr style="background: var(--dark)">
            <th colspan="2">Total</th>
            <td><?= $db->getAnimals("Count(*)", false)->fetchColumn() ?></td>
            <td><?= $db->getAnimals("Count(*)", true)->fetchColumn() ?></td>
            <th></th>
            <th><?= round(($db->getColumn("SELECT Count(*) FROM Animal LEFT JOIN Species ON species=speciesId WHERE dwaa=1") / $db->getColumn("SELECT Count(*) FROM Animal LEFT JOIN Species ON species=speciesId WHERE dwaa=0 OR dwaa=1")) * 100) ?>%</th>
            <th><?= round(($db->getColumn("SELECT Count(*) FROM Animal LEFT JOIN Species ON species=speciesId WHERE ias=1") / $db->getColumn("SELECT Count(*) FROM Animal LEFT JOIN Species ON species=speciesId WHERE ias=0 OR ias=1")) * 100) ?>%</th>
            <th><?= round(($db->getColumn("SELECT Count(*) FROM Animal LEFT JOIN Species ON species=speciesId WHERE isNative=1") / $db->getColumn("SELECT Count(*) FROM Animal LEFT JOIN Species ON species=speciesId WHERE isNative=0 OR isNative=1")) * 100) ?>%</th>
        </tr>

    </table>
</div>

<?php $db->log("Calculating animal intake table"); ?>