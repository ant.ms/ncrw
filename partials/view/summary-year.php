<?php
    include 'partials/view/_common.php';
    $dateRange = isset($_GET['range']) ? $_GET['range'] : date("Y");
?>

<script>
    const switchDate = element => location.href = `?year&range=${element.value}`;
</script>

<div class="row">
    <div class="col s12">
        <div class="right" style="margin-right: 20px">
            <br>
        
            <select onchange="switchDate(this)">
                <?php
                    $cmd = $db->get("SELECT YEAR(dateOfEntry) as year from Animal GROUP BY year ORDER BY year DESC LIMIT 3");
                    $allYearsInDb = [];
                    while ($row = $cmd->fetch()) array_push($allYearsInDb, $row['year']);
                    foreach ($allYearsInDb as $key => $date) {
                        $isSelected = ($date == $dateRange) ? "selected" : "";
                        echo "<option value='$date' $isSelected>$date</option>";
                    }
                ?>
            </select>
        </div>

        <h2>Year Summary</h2><br>
    </div>

    <div class="col s12">

        <?php
            $year = ($dateRange == "*") ? "*" : substr($dateRange, 0, 4);

            function dateCheck(string $column = "dateOfEntry") {
                global $dateRange, $year;
                return ($dateRange == "*") ? "" : " AND YEAR($column)=$year";
            }
            
            include 'partials/view/summary-commons/_index.php';
        ?>


    </div>
</div>