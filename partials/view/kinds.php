<?php
    include 'partials/view/_common.php';

    if ($_SERVER['REQUEST_METHOD'] === 'POST')
        switch ($_POST['command']) {
            case 'add':
                # code...
                break;
            case 'remove':
                # code...
                break;
        }
?>

<style>
    table {
        /* width: calc(100% - 150px); */
        width: 100%;
    }
    .unimportant {
        color: var(--light1) !important
    }
</style>

<div class="row">
<div class="col s12">
    <h2>Kinds of Animal</h2><br>
</div>

<?php
    $countOfAnimalsInDB = $db->getColumn("SELECT Count(*) FROM Animal");

    // Years
    $years = [];
    $yrCmd = $db->get("SELECT YEAR(dateOfEntry) as year FROM Animal GROUP BY year");
    while ($yrRow = $yrCmd->fetch())
        array_push($years, $yrRow['year']);

    $yearQuery = ["Count(*) as total"];
    foreach ($years as $year)
        array_push($yearQuery, "IFNULL(SUM(YEAR(dateOfEntry)=$year), 0) as c$year");

    // Categories
    $categories = [];
    $catCmd = $db->get("SELECT category FROM Species GROUP BY category");
    while ($catRow = $catCmd->fetch())
        array_push($categories, $catRow['category']);
?>

<!-- DWAA -->
    <div class="col" style="margin:0 50px;width: calc(100% - 100px)">
        <div class="card">
            <div class="card-content">
                <table>

                    <!-- Header -->
                        <tr>
                            <th>DWAA species</th>
                            <?php foreach ($years as $year): ?>
                            <th><?= $year ?></th>
                            <?php endforeach; ?>
                            <th>Total</th>
                            <th>%</th>
                        </tr>

                    <!-- Body -->
                        <?php
                        $test = $db->_query([
                            "selector" => array_merge(["category"], $yearQuery),
                            "table" => "Animal",
                            "joins" => ["species"],
                            "group" => "category",
                            "check" => "category IS NOT NULL"
                        ]);
                        while ($testRow = $test->fetch()):
                        ?>
                            <tr>
                                <!-- Category -->
                                <td><?= categoryToString($testRow['category']) ?></td>
                                <!-- Years -->
                                <?php foreach ($years as $year): ?>
                                <td class='<?= ($testRow["c$year"] == '0') ? "unimportant" : "" ?>'>
                                    <?= $testRow["c$year"] ?>
                                </td>
                                <?php endforeach; ?>
                                <!-- Total -->
                                <td class='<?= ($testRow["total"] == '0') ? "unimportant" : "" ?>'>
                                    <b><?= $testRow["total"] ?></b>
                                </td>
                                <!-- Percentage -->
                                <?php $percentage = round( $testRow["total"] / $countOfAnimalsInDB * 100 , 2); ?>
                                <td class='<?= ($percentage == '0') ? "unimportant" : "" ?>'>       <?= $percentage ?>%</td>
                            </tr>
                        <?php endwhile; ?>
                </table>
            </div>
        </div>
    </div>

<!-- Individual Species -->
    <?php foreach (["Large constrictors", "Large reptiles"] as $key => $value): ?>
    <div class="col" style="margin:0 50px;width: calc(100% - 100px)">
        <div class="card">
            <div class="card-content">
                <table>
                    <tr>
                        <th><?= $value ?></th>
                        <?php foreach ($years as $year): ?>
                        <th><?= $year ?></th>
                        <?php endforeach; ?>
                        <th>Total</th>
                        <th>%</th>
                        <!-- <th class="right" style="display: flex;padding-top: 0;padding-bottom: 0">
                            <input type="text" name="" id="">

                            <a href="" class="btn">
                                <i class="material-icons">add</i>
                            </a>
                        </th> -->
                    </tr>
                    <?php
                    $cmd = $db->get("SELECT species, speciesCommonName FROM kindsList LEFT JOIN Species on species=speciesId WHERE section=?", [$key + 1]);
                    while ($row = $cmd->fetch()):
                    ?>
                    <?php
                        $data = $db->query([
                            "selector" => $yearQuery,
                            "table" => "Animal",
                            "check" => "species=?",
                            "args" => [$row['species']],
                            "joins" => ["species"]
                        ])[0];
                    ?>
                    <tr>
                        <td><?= $row['speciesCommonName'] ?></td>
                        <!-- Data -->
                        <?php foreach ($years as $year): ?>
                        <td class='<?= ($data["c$year"] == '0') ? "unimportant" : "" ?>'>   <?= $data["c$year"] ?>      </td>
                        <?php endforeach; ?>
                        <!-- Total -->
                        <td class='<?= ($data["total"] == '0') ? "unimportant" : "" ?>'>    <b><?= $data["total"] ?></b></td>
                        <!-- Percentage -->
                        <?php $percentage = round( $data["total"] / $countOfAnimalsInDB * 100 , 2); ?>
                        <td class='<?= ($percentage == '0') ? "unimportant" : "" ?>'>       <?= $percentage ?>%</td>

                        <!-- <td class="right">
                            <a href="" class="btn">
                                <i class="material-icons">delete</i>
                            </a>
                        </td> -->
                    </tr>
                    <?php endwhile; ?>
                </table>
            </div>
        </div>
    </div>
    <?php endforeach; ?>

</div>

<?php $db->log("Accessed Kinds of Species table") ?>