<?php
    include 'partials/functions/categoryToString.php';
    include 'partials/functions/boolToString.php';
?>

<style>
    tr > td:first-child {
        padding-left: 3rem;
    }

    .click {
        cursor: pointer;
    }

    main {
        padding-right: 0;
        margin-left: -20px;
    }
    tr > th:first-child {
        padding-left: 20px;
    }
</style>

<script>
    let currentlyCollapsed = false;
    const toggleCollapse = categoryId => {
        getElements(`.category${categoryId}`).forEach(element => element.style.display = element.style.display == 'none' ? '' : 'none')
        currentlyCollapsed = !currentlyCollapsed;
    }

    const toggleCollapseAll = () => { getElements('.click').forEach(element => element.click()) }
</script>

<?php
    if ($db->GetUserColumn('ui_grouping') == 1) echo "<script>window.addEventListener('DOMContentLoaded', toggleCollapseAll)</script>";
?>

<!-- <style>
*:not(.modal) .select-wrapper input.select-dropdown {
    text-decoration: none;
    color: var(--light);
    background-color: var(--accent);
    text-align: center;
    letter-spacing: .5px;
    -webkit-transition: background-color .2s ease-out;
    transition: background-color .2s ease-out;
    cursor: pointer;
    font-size: 14px;
    outline: 0;
    border: none;
    border-radius: 2px;
    display: inline-block;
    height: 36px;
    line-height: 36px;
    padding: 0 16px;
    text-transform: uppercase;
    vertical-align: middle;
    -webkit-tap-highlight-color: transparent;
    box-shadow: 0 2px 2px 0 rgb(0 0 0 / 14%), 0 3px 1px -2px rgb(0 0 0 / 12%), 0 1px 5px 0 rgb(0 0 0 / 20%);
} -->
<!-- </style> -->