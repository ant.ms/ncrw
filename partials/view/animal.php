<?php
    include 'partials/functions/sexToString.php';
    include 'partials/functions/boolToString.php';
    include 'partials/functions/countryToString.php';

    if (!$readOnly && isset($_POST['ncrwNumber'])) {
        $db->set("UPDATE Animal SET animalName=? WHERE ncrwRefrenceNumber=?", [ $_POST['newName'], $_POST['ncrwNumber'] ]);
        $db->log("Renamed '".$_POST['ncrwNumber']."' to '".$_POST['newName']."'", 1);
        header('Location: ?view=' . $_POST['ncrwNumber']);
    }

    // get data of animal
    $cmd = $db->get("SELECT * FROM Animal WHERE ncrwRefrenceNumber=?", [$_GET['view']]);
    while ($row = $cmd->fetch()):
?>

<div>

    <div class="col s12">
        <?php if (!$readOnly): ?>
        <div class="right">
            <div style="display: grid;grid-auto-flow: column;align-items:center">         
                <a class="btn modal-trigger" href="#renameModal">
                    <i class="material-icons left">edit</i>Rename
                </a>
            </div>
        </div>
        <?php endif; ?>
        <h2><?= $row['ncrwRefrenceNumber'] ?>: <?= $row['animalName'] ?></h2><br>
    </div>

    <!-- Rename Modal -->
        <div id="renameModal" class="modal">
            <div class="modal-content">
                <h4>Rename '<?= $row['animalName'] ?>'</h4>
                <p>Reference Number: <?= $row['ncrwRefrenceNumber'] ?></p>
                <form action="?view=<?= $row['ncrwRefrenceNumber'] ?>" method="post" id="renameForm">
                    <input type="hidden" name="ncrwNumber" value="<?= $row['ncrwRefrenceNumber'] ?>">
                    <input type="text" name="newName" placeholder="New Name...">
                </form>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close btn-flat" onclick="getElement('renameForm').submit()">Rename</a>
            </div>
        </div>

    <div class="row" id="content">
        <div class="col hide-on-med-and-down l1"></div>

        <div class="col s12 m6 l4">

            <table>

                <tr>
                    <th>NCRW Reference Number</th>
                    <td><?= $row['ncrwRefrenceNumber'] ?></td>
                </tr>

                <tr>
                    <th>Number</th>
                    <td><?= $row['animalNumberCount1'] ?> of <?= $row['animalNumberCount2'] ?></td>
                </tr>

                <tr>
                    <th>Species Common Name</th>
                    <td><?= $db->getColumn("SELECT speciesCommonName FROM Species WHERE speciesId=?", [$row['species']]) ?></td>
                </tr>

                <tr>
                    <th>Species Name</th>
                    <td><?= $db->getColumn("SELECT speciesName FROM Species WHERE speciesId=?", [$row['species']]) ?></td>
                </tr>

                <tr>
                    <th>Morph</th>
                    <td><?= $row['morph'] ?></td>
                </tr>

                <tr>
                    <th>Sex</th>
                    <td><?= sexToLongString($row['sex']) ?></td>
                </tr>

                <tr>
                    <th>Microchip</th>
                    <td><?= $row['microchip'] ?></td>
                </tr>

                <tr>
                    <th>Notes</th>
                    <td><?= $row['notes'] ?></td>
                </tr>

                <tr>
                    <th>Biterisk</th>
                    <td><?= $row['biterisk'] ?></td>
                </tr>

                <tr>
                    <th>Confidence</th>
                    <td><?= $row['confidence'] ?></td>
                </tr>

                <tr>
                    <th>MBD</th>
                    <td><?= $row['mbd'] ?> MBD</td>
                </tr>

                <tr>
                    <th>Rack</th>
                    <td><?= $row['rack'] ?></td>
                </tr>

                <tr>
                    <th>Date of Entry</th>
                    <td><?= $row['dateOfEntry'] ?></td>
                </tr>

                <tr>
                    <th>Date of Birth</th>
                    <td><?= $row['dateOfBirth'] ?></td>
                </tr>

            </table>

        </div>

        <div class="col hide-on-med-and-down l1"></div>

        <div class="col s12 m6 l4">

            <table>

                <tr>

                    <ul class="collapsible">

                        <li>
                            <div class="collapsible-header">
                                <span class="mdi mdi-weight"></span>Weight
                            </div>
                            <div class="collapsible-body subCollapsible">
                                <ul class="collapsible">
                                    <?php
                                        $weightCmd = $db->get("SELECT * FROM WeightMeasurement WHERE referenceNumber=? ORDER BY dateRecorded DESC LIMIT 5", [$row['ncrwRefrenceNumber']]);
                                        while ($weightRow = $weightCmd->fetch()):
                                    ?>
                                    <li>
                                        <div class="collapsible-header">
                                            <?php
                                                $date = new DateTime($weightRow['dateRecorded']);
                                                echo $date->format("jS F Y");
                                                $hourOfDate = (new DateTime($weightRow['dateRecorded']))->format("H:i");
                                                if ($hourOfDate != "00:00") echo " ($hourOfDate)";
                                            ?>
                                        </div>
                                        <div class="collapsible-body">
                                            <span>Recorded weight: <?= $weightRow['recordedWeight'] ?>g</span><br>
                                            <span>Approximate: <?= boolToString($weightRow['isApproximate']) ?></span>
                                        </div>
                                    </li>
                                    <?php endwhile ?>
                                </ul>
                            </div>
                        </li>

                        <li>
                            <div class="collapsible-header">
                                <span class="mdi mdi-tape-measure"></span>Length
                            </div>
                            <div class="collapsible-body subCollapsible">
                                <ul class="collapsible">
                                    <?php
                                        $weightCmd = $db->get("SELECT * FROM LengthMeasurement WHERE referenceNumber=? ORDER BY dateRecorded DESC LIMIT 5", [$row['ncrwRefrenceNumber']]);
                                        while ($weightRow = $weightCmd->fetch()):
                                    ?>
                                    <li>
                                        <div class="collapsible-header">
                                            <?php
                                                $date = new DateTime($weightRow['dateRecorded']);
                                                echo $date->format("jS F Y");
                                                $hourOfDate = (new DateTime($weightRow['dateRecorded']))->format("H:i");
                                                if ($hourOfDate != "00:00") echo " ($hourOfDate)";
                                            ?>
                                        </div>
                                        <div class="collapsible-body">
                                            <span>Recorded Length: <?= $weightRow['recordedLength'] ?>cm</span><br>
                                            <span>Approximate: <?= boolToString($weightRow['isApproximate']) ?></span>
                                        </div>
                                    </li>
                                    <?php endwhile ?>
                                </ul>
                            </div>
                        </li>

                        <li>
                            <div class="collapsible-header">
                                <span class="mdi mdi-spray-bottle"></span>Cleaned
                            </div>
                            <div class="collapsible-body subCollapsible">
                                <ul class="collapsible">
                                    <?php
                                        $weightCmd = $db->get("SELECT * FROM CleanedMeasurement WHERE referenceNumber=? ORDER BY dateRecorded DESC LIMIT 5", [$row['ncrwRefrenceNumber']]);
                                        while ($weightRow = $weightCmd->fetch()):
                                    ?>
                                    <li>
                                        <div class="collapsible-header">
                                            <?php
                                                $date = new DateTime($weightRow['dateRecorded']);
                                                echo $date->format("jS F Y");
                                                $hourOfDate = (new DateTime($weightRow['dateRecorded']))->format("H:i");
                                                if ($hourOfDate != "00:00") echo " ($hourOfDate)";
                                            ?>
                                        </div>
                                    </li>
                                    <?php endwhile ?>
                                </ul>
                            </div>
                        </li>

                        <li>
                            <div class="collapsible-header">
                                <span class="mdi mdi-food-apple"></span>Fed
                            </div>
                            <div class="collapsible-body subCollapsible">
                                <ul class="collapsible">
                                    <?php
                                        $weightCmd = $db->get("SELECT * FROM FedMeasurement WHERE referenceNumber=? ORDER BY dateRecorded DESC LIMIT 5", [$row['ncrwRefrenceNumber']]);
                                        while ($weightRow = $weightCmd->fetch()):
                                    ?>
                                    <li>
                                        <div class="collapsible-header">
                                            <?php
                                                $date = new DateTime($weightRow['dateRecorded']);
                                                echo $date->format("jS F Y");
                                                $hourOfDate = (new DateTime($weightRow['dateRecorded']))->format("H:i");
                                                if ($hourOfDate != "00:00") echo " ($hourOfDate)";
                                            ?>
                                        </div>
                                    </li>
                                    <?php endwhile ?>
                                </ul>
                            </div>
                        </li>

                        <li>
                            <div class="collapsible-header">
                                <span class="mdi mdi-needle"></span>Treatment
                            </div>
                            <div class="collapsible-body subCollapsible">
                                <ul class="collapsible">
                                    <?php
                                        $weightCmd = $db->get("SELECT * FROM TreatmentMeasurement WHERE referenceNumber=? ORDER BY dateRecorded DESC LIMIT 5", [$row['ncrwRefrenceNumber']]);
                                        while ($weightRow = $weightCmd->fetch()):
                                    ?>
                                    <li>
                                        <div class="collapsible-header">
                                            <?php
                                                $date = new DateTime($weightRow['dateRecorded']);
                                                echo $date->format("jS F Y");
                                                $hourOfDate = (new DateTime($weightRow['dateRecorded']))->format("H:i");
                                                if ($hourOfDate != "00:00") echo " ($hourOfDate)";
                                            ?>
                                        </div>
                                        <div class="collapsible-body">
                                            <span><?= $weightRow['notes'] ?></span><br>
                                        </div>
                                    </li>
                                    <?php endwhile ?>
                                </ul>
                            </div>
                        </li>

                    </ul>

                </tr>

                <tr>
                    <th>Donor</th>
                    <td style="display: grid;">
                        <span><?= $row['personDonor'] ?></span>
                        <?php if (!$readOnly): ?>
                            <span><?= $row['personName'] ?></span>
                            <span><?= $row['personEmail'] ?></span>
                            <span><?= $row['personPhone'] ?></span>
                        <?php endif; ?>
                    </td>
                </tr>

                <tr>
                    <th>Location</th>
                    <td style="display: grid;">
                        <span><?= countryToString($row['country']) ?></span>
                        <span><?= $row['postcode'] ?></span>
                    </td>
                </tr>

                <tr>
                    <th>Source</th>
                    <td><?= $row['source'] ?></td>
                </tr>

                <tr>
                    <th><?= $row['isThirdParty'] == 1 ? "Veterinary history" : "Background" ?></th>
                    <td><?= $row['background'] ?></td>
                </tr>

                <tr>
                    <th>Type</th>
                    <th><?= ($row['isThirdParty'] == 1) ? "Third Party" : "Owner Relinquishment" ?></th>
                </tr>

                <?php if ($row['isThirdParty'] == 1): ?>

                    <tr>
                        <th>Origin</th>
                        <td><?= $row['origin'] ?></td>
                    </tr>

                <?php else: ?>

                    <tr>
                        <th>Reason for Rehoming</th>
                        <td><?= $row['reasonForRehoming'] ?></td>
                    </tr>

                    <tr>
                        <th>DurationOwned</th>
                        <td><?= $row['durationOwned'] ?></td>
                    </tr>

                <?php endif; ?>

                <tr>
                    <th>How did they find us?</th>
                    <td><?= $row['referral'] ?></td>
                </tr>

                <tr>
                    <td colspan="2">
                        <label>
                            <input type="checkbox" <?= (($row['mites'] == 1) ? "checked" : "") ?> onclick="return false">
                            <span>Mites</span>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <label>
                            <input type="checkbox" <?= (($row['dwaa'] == 1) ? "checked" : "") ?> name="dwaa" onclick="return false">
                            <span>DWAA</span>
                        </label>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="2">
                    <label>
                        <input type="checkbox" <?= (($row['ias'] == 1) ? "checked" : "") ?> name="ias" onclick="return false">
                        <span>IAS</span>
                    </label>
                </tr>
                
                <tr>
                    <td colspan="2">
                    <label>
                        <input type="checkbox" <?= (($row['euthanised'] == 1) ? "checked" : "") ?> name="euthanised" onclick="return false">
                        <span>Euthanised</span>
                    </label>
                </tr>
                
                <tr>
                    <td colspan="2">
                    <label>
                        <input type="checkbox" <?= (($row['has_article10'] == 1) ? "checked" : "") ?> name="article10" onclick="return false">
                        <span>Article 10</span>
                    </label>
                </tr>
                
                <tr>
                    <td colspan="2">
                    <label>
                        <input type="checkbox" <?= (($row['has_giftCert'] == 1) ? "checked" : "") ?> name="giftCert" onclick="return false">
                        <span>Gift Certificate</span>
                    </label>
                </tr>
                
            </table>

        </div>
    </div>

</div>
<?php endwhile; ?>

<style>
.mdi {
    line-height: 1;
    letter-spacing: normal;
    text-transform: none;
    white-space: nowrap;
    word-wrap: normal;
    direction: ltr;
    width: 2rem;
    font-size: 1.6rem;
    display: inline-block;
    text-align: center;
    margin-right: 1rem;
}

.subCollapsible {
    padding: 10px;
}

.subCollapsible > .collapsible {
    margin: 0;
}

span {
    cursor: unset !important;
}
</style>