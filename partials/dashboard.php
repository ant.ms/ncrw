<?php
    include 'partials/functions/categoryToString.php';

    function getWelcomeMessage() {
        switch (date('G')) {
            case 23:
            case 0:
            case 1:
            case 3:
                return "Up late?";

            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                return "Good Morning,";
                
            case 11:
            case 12:
            case 13:
                return "Hello,";

            case 14:
            case 15:
            case 16:
            case 17:
                return "Good Afternoon,";

            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
                return "Good Evening";
            
            default:
                return "Welcome,";
        }
    }
?>

<br>

<div class="row">

    <!-- Welcome Card -->
    <div class="col s12">

        <div class="card">
            <div class="card-content row">

                <div class="col s12 m6">
                    <span class="card-title" style="font-size: 30px; margin: 0"><?= getWelcomeMessage() ?> <?= $db->getUserColumn("firstname") ?>!</span>
                </div>
                
                <div class="col s12 m6">
                    <div class="right" style="margin: -5px 0">
                        <span class="right">Today is <?= date('l\, \t\h\e jS \of F Y') ?></span><br>
                        <span class="right" id="weatherElement"></span>
                        <script>
                            
                            fetch('https://api.openweathermap.org/data/2.5/weather?q=Tonbridge&units=metric&appid=ffc901dac7ac1f163ecfd9bb47c021ab')
                            .then(function(resp) { return resp.json() })
                            .then(function(data) {
                                getElement('weatherElement').innerText = `It's ${Math.round(data.main.temp)}°C with ${data.weather[0].description}`;
                            })

                        </script>
                    </div>
                </div>
                
            </div>
        </div>

    </div>

    <!-- Species Card -->
    <div class="col s12 m6">

        <div class="card">
            <div class="card-content">
                <span class="card-title">Species</span>
                <table>
                    <tr>
                        <th>Category</th>
                        <th>Species</th>
                        <th>Animals</th>
                    </tr>
                    
                    <?php
                        $cmd = $db->get("SELECT category FROM Species GROUP BY category");
                        while ($row = $cmd->fetch()):
                    ?>
                        <tr>
                            <th><?= categoryToString($row['category']) ?></th>
                            <td><?= $db->getColumn("SELECT COUNT(*) FROM Species WHERE category=?", [$row['category']]) ?></td>
                            <td><?= $db->getColumn("SELECT COUNT(*) FROM Animal LEFT JOIN Species ON species=speciesId WHERE category=?", [$row['category']]) ?></td>
                        </tr>
                    <?php endwhile ?>

                </table>
            </div>

            <div class="card-action">
                <span style="margin-top: 5px;">Total: <?= $db->getColumn("SELECT COUNT(*) FROM Species") ?></span>
                <a href="?species" class="btn"><i class="material-icons left">pageview</i> View</a>
            </div>
        </div>

    </div>

    <!-- Animals Card -->
    <div class="col s12 m6">

        <div class="card">
            <div class="card-content">
                <span class="card-title">Animals</span>
                <table>
                    <tr>
                        <th>Category</th>
                        <th>Total</th>
                        <th>Dead</th>
                        <th>Rehomed</th>
                    </tr>
                    
                    <?php
                        $cmd = $db->get("SELECT category FROM Species GROUP BY category");
                        while ($row = $cmd->fetch()):
                    ?>
                        <tr>
                            <th><?= categoryToString($row['category']) ?></th>
                            <td><?= $db->getColumn("SELECT COUNT(*) FROM Animal LEFT JOIN Species on species=speciesId WHERE category=?", [$row['category']]) ?></td>
                            <td><?= $db->getColumn("SELECT COUNT(*) FROM Animal LEFT JOIN Species on species=speciesId LEFT JOIN mortality ON ncrwRefrenceNumber=mortality.referenceNumber WHERE mortality.referenceNumber IS NOT NULL AND category=?", [$row['category']]) ?></td>
                            <td><?= $db->getColumn("SELECT COUNT(*) FROM Animal LEFT JOIN Species on species=speciesId LEFT JOIN rehoming ON ncrwRefrenceNumber=rehoming.referenceNumber WHERE rehoming.referenceNumber IS NOT NULL AND category=?", [$row['category']]) ?></td>
                        </tr>
                    <?php endwhile ?>

                </table>
            </div>

            <div class="card-action">
                <span style="margin-top: 5px;">Total: <?= $db->getColumn("SELECT COUNT(*) FROM Animal") ?></span>
                <a href="?animals" class="btn"><i class="material-icons left">pageview</i> View</a>
            </div>
        </div>
        
    </div>


    <!-- Top 10 Species -->
    <div class="col s12">
        <div class="card">
            <div class="card-content" style="overflow:scroll">
                <span class="card-title">Top 10 Species</span>
                <table>
                    <tr>
                        <th>Species</th>

                        <?php
                        // Get years in DB
                        $cmd = $db->get("SELECT YEAR(dateOfEntry) as year from Animal GROUP BY year ORDER BY year ASC LIMIT 3");
                        $allYearsInDb = [];
                        while ($row = $cmd->fetch()) array_push($allYearsInDb, $row['year']);

                        foreach ($allYearsInDb as $key => $year):
                        ?>

                            <th><?= $year ?></th>

                        <?php endforeach ?>

                        <th>Total</th>
                    </tr>
                    
                    <?php
                    // Get The top 10 species
                    $cmd = $db->get("SELECT COUNT(*) as countInSpecies, speciesCommonName, species FROM Animal LEFT JOIN Species ON species=Species.speciesId GROUP BY species ORDER BY countInSpecies DESC LIMIT 10");
                    while ($speciesRow = $cmd->fetch()):
                    ?>
                        <tr>
                            <td><?= $speciesRow['speciesCommonName'] ?></td>

                            <?php
                            foreach ($allYearsInDb as $key => $year):
                            ?>

                                <?php
                                // Get data for species and year
                                echo "<td>" . $db->getColumn("SELECT COUNT(*) FROM Animal LEFT JOIN Species ON species=Species.speciesId WHERE species=? AND YEAR(dateOfEntry)=?", [$speciesRow['species'], $year]) . "</td>";
                                ?>

                            <?php endforeach ?>

                            <td><b><?= $speciesRow['countInSpecies'] ?></b></td>
                        </tr>
                    <?php endwhile ?>

                    <!-- <tr>
                        <th>Totals</th>
                        <th>124</th>
                        <th>456</th>
                        <th>823</th>
                        <th>1234</th>
                    </tr> -->
                </table>
            </div>
        </div>
        
    </div>

</div>

<?php $db->log("Accessed Dashboard") ?>