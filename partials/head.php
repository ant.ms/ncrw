<meta charset="UTF-8">
<meta name="description" content="National Centre for Reptile Welfare (NCRW) Database, stores all the data of adorable animals">
<meta name="keywords" content="HTML, CSS, JavaScript, Material">
<meta name="author" content="Yanik Ammann">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="logo.png" type="image/ong">

<?php
    if (isset($_SESSION['username'])) {
        if ($db->getUserColumn("ui_theme") == "auto") {
            echo "<style>@media (prefers-color-scheme: dark) {";
                include 'css/colors/dark.css';
            echo "}</style>";
            echo "<style>@media (prefers-color-scheme: light) {";
                include 'css/colors/light.css';
            echo "}</style>";
        } else {
            echo "<link rel='stylesheet' href='css/colors/{$db->getUserColumn("ui_theme")}.css'>";
        }
        echo "<link rel='stylesheet' href='css/colors/accent/{$db->getUserColumn("ui_accent")}.css'>";
    } else {
        echo "<link rel='stylesheet' href='css/colors/dark.css'>";
        echo "<link rel='stylesheet' href='css/colors/accent/blue.css'>";
    }

    $readOnly = $db->getColumn("SELECT is_administrator FROM users WHERE username=?", [$_SESSION['username']]) == 2;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@mdi/font@5.9.55/css/materialdesignicons.min.css">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/font.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/input.css">
<link rel="stylesheet" href="css/sidebar.css">

<script src="js/utils.js" defer></script>