<?php
    include '_root.php';
    include __DIR__.'/../functions/citesToString.php';

    foreach (['A', 'B', 'C', 'D'] as $value) {
        (new Test("Get Annex $value"))
        ->expect(citesToString("$value"))
        ->toBe("Annex $value");
    }

    (new Test("Wrong value"))
    ->expect(citesToString('asd'))
    ->toBe("No Annex / Unknown");

    endTestGroup();
?>