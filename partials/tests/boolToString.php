<?php
    include '_root.php';
    include __DIR__.'/../functions/boolToString.php';

    (new Test("String from Int"))
    ->expect(boolToString(1))
    ->toBe("yes");
    (new Test("String from Bool"))
    ->expect(boolToString(false))
    ->toBe("no");

    (new Test("Icon from Int"))
    ->expect(boolToIcon(0))
    ->toBe("<span class='mdi mdi-close'></span>");
    (new Test("Icon from Bool"))
    ->expect(boolToIcon(true))
    ->toBe("<span class='mdi mdi-check'></span>");

    endTestGroup();
?>