<?php
    if (php_sapi_name() != "cli") die("This script should not be executed from the browser!\n");
    
    function out($str, $type = 'i') {
        switch ($type) {
            case 'e': //error
                echo "\033[31mFAILED\033[0m\t$str\n";
                break;
            case 's': //success
                echo "\033[32mSUCCESS\033[0m\t$str\n";
                break;
            case 'w': //warning
                echo "\033[33mWARNING\033[0m\t$str\n";
                break;  
            default: //info
                echo "INFO\t$str";
                break;
        }
    }

    $successfulTests = 0;
    $failedTests = 0;

    class Test{
        protected $name;
        protected $value;

        private function success() {
            global $successfulTests;
            out($this->name, 's');
            $successfulTests++;
        }
        private function failure($expectation) {
            global $failedTests;
            out($this->name, 'e');
            echo "Expected '$expectation', but got '$this->value'\n";
            $failedTests++;
        }
        function __construct($name) {
            $this->name = $name;
        }

        public function expect($value) {
            $this->value = $value;
            return $this;
        }

        public function toBe($value) {
            if ($this->value === $value)
                $this->success();
            else
                $this->failure($value);
        }
        public function toNotBe($value) {
            if ($this->value !== $value)
                $this->success();
            else
                $this->failure($value);
        }

        public function toEqual($value) {
            if ($this->value == $value)
                $this->success();
            else
                $this->failure($value);
        }
    }

    function endTestGroup() {
        global $successfulTests;
        global $failedTests;
        $totalTests = $successfulTests + $failedTests;
        $successPercentage = round($successfulTests/$totalTests * 100);

        echo "$successfulTests out of $totalTests tests passed ($successPercentage%)\n";
        // Tell CI, that this test failed
        if ($failedTests > 0) exit(69);
    }
?>