<?php
    include '_root.php';

    include __DIR__.'/../database.php';
    $db = new Database;

    include __DIR__.'/../functions/createUser.php';
    include __DIR__.'/../functions/toggleAdmin.php';

    $_SESSION['username'] = "Beep Boop";

    $previousUserCount = $db->getColumn("SELECT count(*) FROM users");

    // createUser(string $new_username, string $new_firstname, string $new_lastname, string $new_password, bool $isadmin = false)
    (new Test("User creation"))
    ->expect( createUser("Cutie", "Julien", "Acker", "I-am-Cute", false) )
    ->toBe(true);

    (new Test("User stored in db"))
    ->expect( $db->getColumn("SELECT count(*) FROM users") )
    ->toEqual( $previousUserCount + 1 );

    (new Test("User is not admin"))
    ->expect( $db->getColumn("SELECT is_administrator FROM users WHERE username='Cutie'") )
    ->toEqual('0');

    toggleAdmin("Cutie");
    
    (new Test("Make user admin"))
    ->expect( $db->getColumn("SELECT is_administrator FROM users WHERE username='Cutie'") )
    ->toEqual('1');

    toggleAdmin("Cutie");
    
    (new Test("Make user view_only"))
    ->expect( $db->getColumn("SELECT is_administrator FROM users WHERE username='Cutie'") )
    ->toEqual('2');

    endTestGroup();
?>