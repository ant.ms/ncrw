<?php
    include '_root.php';
    include __DIR__.'/../functions/categoryToString.php';

    (new Test("Get Snake"))
    ->expect(categoryToString(1))
    ->toBe("Snake");

    (new Test("Get Lizards"))
    ->expect(categoryToString(2))
    ->toBe("Lizards");

    (new Test("Get Tortoises"))
    ->expect(categoryToString(3))
    ->toBe("Tortoises");

    (new Test("Get Turtles"))
    ->expect(categoryToString(4))
    ->toBe("Turtles");

    (new Test("Get Crocodilians"))
    ->expect(categoryToString(5))
    ->toBe("Crocodilians");

    (new Test("Get Amphibians"))
    ->expect(categoryToString(6))
    ->toBe("Amphibians");

    (new Test("Get Invertebrates"))
    ->expect(categoryToString(7))
    ->toBe("Invertebrates");

    (new Test("Get non existing value"))
    ->expect(categoryToString(8))
    ->toBe("Unknown");


    endTestGroup();
?>