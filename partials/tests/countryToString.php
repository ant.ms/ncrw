<?php
    include '_root.php';
    include __DIR__.'/../functions/countryToString.php';

    (new Test("Get Switzerland"))
    ->expect(countryToString("CH"))
    ->toBe("Switzerland");

    (new Test("Get United Kingdom"))
    ->expect(countryToString("GB"))
    ->toBe("United Kingdom");

    (new Test("Wrong value"))
    ->expect(countryToString('AB'))
    ->toBe("AB");

    endTestGroup();
?>