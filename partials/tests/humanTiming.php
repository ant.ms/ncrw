<?php
    include '_root.php';
    include __DIR__.'/../functions/humanTiming.php';

    (new Test("5 Years"))
    ->expect(humanTiming(strtotime("-5 years")))
    ->toBe("5 years");

    (new Test("1 Month"))
    ->expect(humanTiming(strtotime("-1 month")))
    ->toBe("1 month");

    (new Test("4 Weeks"))
    ->expect(humanTiming(strtotime("-4 weeks")))
    ->toBe("4 weeks");

    (new Test("7 Hours"))
    ->expect(humanTiming(strtotime("-7 hours")))
    ->toBe("7 hours");

    (new Test("8 Minutes"))
    ->expect(humanTiming(strtotime("-8 minutes")))
    ->toBe("8 minutes");

    endTestGroup();
?>