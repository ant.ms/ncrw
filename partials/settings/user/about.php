<div class="row">
    <div class="col s12">
        <h2>National Centre for Reptile Welfare (NCRW)</h2>
    </div>
    
    <div class="col s12 m8 l9">
        <div class="row">
            <div class="col 12">
                <p>
                    This database software, is a project project, that I'm doing for a amazing and great friend that works at the National Centre for Reptile Welfare (NCRW).
                </p>
                <p>
                    It's written in PHP and uses an SQL database. All information and data is stored on the SQL database.
                    If you want to update the project, you can just <a href="https://www.git-scm.com/docs/git-pull">pull</a> the php-root. Please make regular backups of the database, either manually, or through the <a href="/settings.php?page=export">Export settings page</a> (requires Administrator priviledges). You can also setup a crontab or something similar, to backup the entire SQL-Server to a (remote) location.
                </p>
                <p>
                    <h5>Liability and legal</h5>
                    <p>
                        This project is <a href="https://en.wikipedia.org/wiki/Open_source">Open Source</a> and can be viewed on it's <a href="https://gitlab.com/ConfusedAnt/ncrw">official repo</a> (please ask me before forking/using it, I would love to hear what my projects are being used for).<br>
                        
                        No guarantee or liability for any damage that may be caused is provived for this project, use at your own risk.
                    </p>
                </p>
            </div>
        </div>
    </div>
    
    <div class="col hide-on-small-only m4 l3">
        <br>
        <img class="responsive-img" src="/logo.png">
    </div>

    <div class="col s12">
        <h5 style="margin-top: -17px">Support</h5>
        <p>
            This project was created by Yanik Ammann (me, sometimes known as ConfusedAnt or 1Yanik3). You can find more projects from me on my website: <a href="https://ant.lgbt/">ant.lgbt</a>.
        </p>
        <p>
            If you need any support, I am more than willing to help, just message me under <a href="mailto:confused@ant.lgbt">confused@ant.lgbt</a> (I speak Swiss-German, English, and German), or create an issue on the projects <a href="https://gitlab.com/ConfusedAnt/ncrw">official repo</a>.
        </p>
    </div>
</div>