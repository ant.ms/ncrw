<link rel="stylesheet" href="partials/settings/admin/export.css">
<?php $dump = $db->getDump() ?>

<div class="row" id="content">
    <div class="col s12">
        <div class="right">
            <br>
            <a class="btn" onclick="copyElementToClipboard(document.getElementById('rawExportText'))"><i class="material-icons left">content_copy</i>Copy to clipboard</a>
            <a class="btn" onclick="downloadStringAsFile(document.getElementById('rawExportText').innerHTML)"><i class="material-icons left">download</i>Download as .sql</a>
        </div>
        <h2>Export</h2>
        <br>
        This is an sqldump of the current state of the database. Please save this to a file somewhere as a backup or in order to migrate this install.
    </div>
    <div class="col s12">
        <pre><code class="z-depth-5">
<?= $dump ?>
        </code></pre>
    </div>
</div>
<div style="overflow:hidden; height:1px; width:1px">

<textarea  readonly id="rawExportText" style="width:0px;overflow:hidden"><?= $dump ?></textarea>

<script src="partials/settings/admin/export.js"></script>
<script>hljs.initHighlightingOnLoad();</script>
<script src="js/utils.js"></script>

<?php  $db->log("Created database export", 2); ?>