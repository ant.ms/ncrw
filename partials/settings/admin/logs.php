<?php
    include 'partials/functions/humanTiming.php';
    
    function logToString(int $value) {
        switch ($value) {
            case 0:
                return "info";
            case 1:
                return "log";
            case 2:
                return "error";
            case 3:
                return "critical";
            
            default:
                return "unknown";
        }
    }

?>

  <!-- Dropdown Structure -->
  <ul id='logsDropdown' class='dropdown-content'>
    <li><a href="settings.php?logs">Show 50 (default)</a></li>
    <li><a href="settings.php?logs&count=250">Show 250</a></li>
    <li><a href="settings.php?logs&count=500">Show 500</a></li>
    <li><a href="settings.php?logs&count=2500">Show 2500</a></li>
    <li><a href="settings.php?logs&count=5000">Show 5000</a></li>
  </ul>

<div class="row" id="content">
    <div class="col s12">
        <div class="right">
            <br>
            <a class='dropdown-trigger btn' href='#' data-target='logsDropdown'><i class="material-icons">arrow_drop_down</i></a>
        </div>
        <h2>Logs</h2><br>
    </div>
    <div class="col s12">
        <div class="row">

            <table class="striped">
                <tr>
                    <th>Time</th>
                    <th>Importance</th>
                    <th>Content</th>
                    <th>Duration</th>
                    <th>User</th>
                </tr>

                <?php
                    $limit = 50;
                    if (isset($_GET['count']))
                        $limit = $_GET['count'];

                    $cmd = $db->get("SELECT * FROM logs ORDER BY dateLogged DESC LIMIT $limit");
                    while ($row = $cmd->fetch()):
                ?>

                    <tr <?= ($row['importance'] == 0 ? "class='unimportant'" : "") ?>>
                        <td class="tooltipped" data-tooltip="<?= $row['dateLogged'] ?>"><?= humanTiming(strtotime($row['dateLogged'])) ?> ago</td>
                        <td><?= logToString($row['importance']) ?></td>
                        <td><?= $row['content'] ?></td>
                        <td><?= round($row['duration'] * 1000) ?>ms</td>
                        <td><?= $row['username'] ?></td>
                    </tr>

                <?php endwhile; ?>
            </table>

        </div>
    </div>
</div>

<style>
    .unimportant > td {
        filter: contrast(0.15);
    }
</style>

<?php $db->log("Listing logs in Database"); ?>