<?php
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {

        if (isset($_GET['error']))
            echo "<script>document.addEventListener('DOMContentLoaded', function() {M.toast({html: 'Something went wrong, the user couldnt be added :('});});</script>";

        if (isset($_GET['setBackground'])) {
            $db->set('UPDATE globalSettings SET login_bgPicture=?', [$_GET['setBackground']]);
            $db->log("Updated loginscreen background to '" . $_GET['setBackground'] . "'", 1);
        }

        if (isset($_GET['setForeground'])) {
            $db->set('UPDATE globalSettings SET login_fgPicture=?', [$_GET['setForeground']]);
            $db->log("Updated loginscreen foreground to '" . $_GET['setForeground'] . "'", 1);
        }

        echo "<script>history.pushState({}, '', 'settings.php?general');</script>";
    }

    $db_bgImage = $db->getColumn('SELECT login_bgPicture FROM globalSettings');
    $db_fgImage = $db->getColumn('SELECT login_fgPicture FROM globalSettings');
?>

<link rel="stylesheet" href="partials/settings/admin/general.css">

<div class="row" id="content">
    <div class="col s12">
        <h2>General</h2>
    </div>
    
    <div>
        <div class="col s12">
            <div class="col s12">
                <h5>Foreground</h5>
                <?php foreach (array_slice(scandir("img/loginPage/"), 2) as $value): ?>
                    <div class="col s12 m6 l4 radioCards">
                        <div class="card <?= ($value == $db_fgImage) ? "selectedRadioCard" : "" ?>" onclick="document.location.href='settings.php?general&setForeground=<?= $value ?>'">
                            <div class="card-image">
                                <img src="img/loginPage/<?= $value; ?>">
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>

            <div class="col s12">
                <h5>Background</h5>
                <?php foreach (array_slice(scandir("img/loginPage/"), 2) as $value): ?>
                    <div class="col s12 m6 l4 radioCards">
                        <div class="card <?= ($value == $db_bgImage) ? "selectedRadioCard" : "" ?>" onclick="document.location.href='settings.php?general&setBackground=<?= $value ?>'">
                            <div class="card-image">
                                <img src="img/loginPage/<?= $value ?>">
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>