function validateAddUser() {
    let everythingCorrect = true;
    document.querySelectorAll('.requiredToProceed').forEach(element => {
        if (element.classList.contains('invalid'))
            everythingCorrect = false;
    });

    if (everythingCorrect == true)
        document.getElementById('addUserButton').classList.remove('disabled');
    else
        document.getElementById('addUserButton').classList.add('disabled');
}