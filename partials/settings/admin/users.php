<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        include 'partials/functions/createUser.php';
        
        // create user and react to error/success
        if (createUser($_POST['username'], $_POST['firstname'], $_POST['lastname'], $_POST['password'], false))
            header('Location: settings.php?users');
        else
            header('Location: settings.php?users&error');
        exit;

    } else if ($_SERVER['REQUEST_METHOD'] === 'GET') {

        if (isset($_GET['error'])) {
            echo "<script>document.addEventListener('DOMContentLoaded', function() {M.toast({html: 'Something went wrong, the user couldnt be added :('});});</script>";
        }

        if (isset($_GET['toggleAdmin'])) {
            include 'partials/functions/toggleAdmin.php';
            toggleAdmin($_GET['toggleAdmin']);
        }

        if (isset($_GET['deleteUser'])) {
            $db->set('DELETE FROM users WHERE username=?', [$_GET['deleteUser']]);
            $db->log("Deleted user: '" . $_GET['deleteUser'] . "'", 2);
        }

        echo "<script>history.pushState({}, '', 'settings.php?users');</script>";
    }
?>

<link rel="stylesheet" href="partials/settings/admin/users.css">

<div class="row" id="content">
    <div class="col s12">
        <div class="right">
            <br>
            <a class="waves-effect waves-teal btn modal-trigger" href="#addUser">
                <i class="material-icons left">person_add</i>
                Add new User
            </a>
        </div>
        <h2>Users</h2>
    </div>
    <div class="col s12">
        <div class="row">
            <?php
                $cmd = $db->get("SELECT * FROM users");
                while ($row = $cmd->fetch()):
            ?>
                <div class="col s12 m6 l4">
                    <div class="card z-depth-2">
                        <div class="card-content" style="display: flex;justify-content:space-between">
                            <div>
                                <span class="card-title"><?= $row['username'] ?></span>
                                <p><?= $row['firstname'] . " " . $row['lastname'] ?></p>
                            </div>

                            <div>
                                <p style="float:right">
                                    <?php
                                        if ($row['is_administrator'] == '1')
                                            echo "<i class='material-icons tooltipped' data-position='left' data-tooltip='Administrator'>supervisor_account</i>";
                                        else if ($row['is_administrator'] == '0')
                                            echo "<i class='material-icons tooltipped' data-position='left' data-tooltip='Normal User'>person</i>";
                                        else
                                            echo "<i class='material-icons tooltipped' data-position='left' data-tooltip='View Only User'>pageview</i>";
                                    ?>
                                </p><br>
                                <p style="float:right">
                                    <i class="material-icons tooltipped" data-position='left' data-tooltip="UI: theme <?= $row['ui_theme'] ?>">style</i>
                                </p>
                            </div>
                        </div>

                        <!-- START: Make sure you can't administer yourself -->
                        <?php if ($row['username'] != $_SESSION['username']): ?>
                        <div class="card-action">
                            <a class="btn <?= ($row['is_administrator'] == 1) ? "highlighted" : "" ?>" href="settings.php?users&toggleAdmin=<?= $row['username'] ?>">
                                <?php
                                    if ($row['is_administrator'] == '1')
                                        echo "<i class='material-icons tooltipped' data-tooltip='Administrator'>supervisor_account</i>";
                                    else if ($row['is_administrator'] == '0')
                                        echo "<i class='material-icons tooltipped' data-tooltip='Normal User'>person</i>";
                                    else
                                        echo "<i class='material-icons tooltipped' data-tooltip='View Only User'>pageview</i>";
                                ?>
                            </a>
                            <div>
                                <a onclick="document.getElementById('deleteModalButton').href = 'settings.php?users&deleteUser=<?= $row['username'] ?>'" href="#deleteUser" class="tooltipped btn modal-trigger" data-tooltip="Delete this user"><i class="material-icons">delete_forever</i></a>
                            </div>
                        </div>
                        <?php else: ?>
                            <div class="card-action" style="height: 69px;display:flex;justify-content:center;align-items:center">
                                Sorry, you can't administer yourself
                            </div>
                        <?php endif; ?>
                        <!-- END: Make sure you can't administer yourself -->
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</div>

<!-- Delete Modal -->
<div id="deleteUser" class="modal">
    <div class="modal-content">
        <h4>Delete User?</h4>
        <p>Do you really want to delete this user?</p>
        <p>This will only delete the user, all notes and shares of this user will NOT be deleted.</p>
    </div>
    <div class="modal-footer">
        <a class="modal-close waves-effect waves-green btn-flat left"><i class="material-icons left">cancel</i>Cancel</a>
        <a id="deleteModalButton" href="" class="waves-effect waves-green btn-flat"><i class="material-icons left">delete_forever</i>Delete FOREVER (A really long time)</a>
    </div>
</div>

<!-- Modal Structure -->
<div id="addUser" class="modal">
    <div class="modal-content">
        <h4>Add User</h4>
        <form class="row" action="settings.php?users" method="post" id="userForm">
            <div class="input-field col s12">
                <input name="username" id="username" type="text" class="requiredToProceed invalid" oninput="checkLength(this, true, 25, 5);validateAddUser()">
                <label for="username">Username (Between 5 and 25 characters)</label>
            </div>
            <div class="input-field col s12 l6">
                <input name="firstname" id="first_name" type="text" class="requiredToProceed invalid" oninput="checkLength(this, true, 64, 3);validateAddUser()">
                <label for="first_name">First Name (Between 3 and 64 characters)</label>
            </div>
            <div class="input-field col s12 l6">
                <input name="lastname" id="last_name" type="text" class="requiredToProceed invalid" oninput="checkLength(this, true, 64, 3);validateAddUser()">
                <label for="last_name">Last Name (Between 3 and 64 characters)</label>
            </div>
            <div class="input-field col s12">
                <input name="password" id="password" type="password" class="requiredToProceed invalid" oninput="checkLength(this, false, 1024, 8);validateAddUser()">
                <label for="password">Password (At least 8 characters)</label>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a class="modal-close waves-effect waves-green btn-flat left"><i class="material-icons left">cancel</i>Cancel</a>
        <a onclick="document.getElementById('userForm').submit()" id="addUserButton" class="waves-effect waves-green btn right disabled"><i class="material-icons left">person</i>Add User</a>
    </div>
</div>


<script src="partials/settings/admin/users.js"></script>
<script src="js/utils.js"></script>

<?php $db->log("Listing users in database", 0); ?>