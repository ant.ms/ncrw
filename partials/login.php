<?php
    // create connection to Database
	$db = new Database;

    if (isset($_GET['logout'])) {
        // remove all session variables and destroy the session
        session_unset();
        session_destroy(); 

        echo "<script>document.addEventListener('DOMContentLoaded', function() {M.toast({html: 'You have been logged out!'});});</script>";
    }

    else if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        // Check if login is coorect
        $cmd = $db->get('SELECT * FROM users WHERE username=:username AND passwd=:passwd', ['username' => $_POST['username'], 'passwd' => hash('sha256', "This text has been salted and hashed" . $_POST['password'] . $_POST['username'])]);
        if ($cmd->fetchAll()) {
            $_SESSION['loggedIn'] = true;
            $_SESSION['username'] = $_POST['username'];

            // reload page to get rid of POST content (in case the user wants to reload the page)
            header('Location: .');
            exit;
        } else {
            echo "<script>document.addEventListener('DOMContentLoaded', function() {M.toast({html: 'This didn\'t work! Did you enter the correct login?'});});</script>";
        }
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>NCRW - Login</title>

        <?php include 'partials/head.php' ?>
        <link rel="stylesheet" href="css/_login.css">

        <style>#wrapper { background: linear-gradient(to bottom right, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.95)), url('/img/loginPage/<?= $db->getColumn('SELECT login_bgPicture FROM globalSettings') ?>') }</style>
    </head>

    <body>
    <div id="wrapper">
        <div style="height:25vh"></div>
            <div class="row">
                <div class="col s0 m2"></div>
                <div class="col s12 m8">
                    <div class="card install z-depth-5 center" style="max-width: 960px; margin: auto;">
                        <div class="card-content row">
                            <div class="col s12 l5">
                                <form class="col s12" action="." method="POST">
                                    <h1>NCRW</h1>
                                    <div class="input-field col s12">
                                        <input name="username" id="username" type="text" class="validate">
                                        <label for="username">Username</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <input name="password" id="password" type="password" class="validate">
                                        <label for="password">Password</label>
                                    </div>
                                    <button class="btn waves-effect waves-light right" type="submit" name="action">Login
                                        <i class="material-icons right">send</i>
                                    </button>

                                </form>
                            </div>

                            <div class="col s12 l7 hide-on-med-and-down">
                                <img src="img/loginPage/<?= $db->getColumn('SELECT login_fgPicture FROM globalSettings') ?>" alt="" class="responsive-img" style="height:340px">
                            </div>

                            <div style="clear:both"></div>
                        </div>
                    </div>
                </div>
                <div class="col s0 m2"></div>
            </div>
        </div>

        <?php include 'partials/scripts.php' ?>
    </body>
</html>