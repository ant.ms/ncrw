<?php
include 'partials/startup.php';
require 'partials/functions/umami.php';
require 'partials/functions/generateMenuEntry.php';
?>
<!DOCTYPE html>
<html>
    <head>
            <title>National Centre for Reptile Welfare (NCRW) - Settings</title>
            
            <?php include 'partials/head.php' ?>

            <link rel="stylesheet" href="css/_pages.css">
            <link rel="stylesheet" href="css/_settings.css">
    </head>

    <body>
        <div id="wrapper">
        <?php include 'partials/navbar.php'; ?>

        <ul id="slide-out" class="sidenav sidenav-fixed z-depth-5">
            <li><h4 class="sidebarTitle">Settings</h4></li>
            <?php
                genMenuEntry("Theming",     "theme");
                genMenuEntry("About",       "about");
            
                // Display Administration settings, only if user is an administrator
                $isAdmin = false;
                $cmd = $db->get("SELECT * FROM users WHERE username=? AND is_administrator=1", [$_SESSION["username"]]);
                if ($cmd->fetchAll()) {
                    $isAdmin = true;
                    echo "<li><h5 class='sidebarTitle'>Administration</h5></li>";
                    genMenuEntry("General",         "general");
                    genMenuEntry("Users",           "users");
                    genMenuEntry("Logs",            "logs");
                    genMenuEntry("Backup / Export", "export");
                }
            ?>


            <?php if ($isAdmin): ?>
            <!-- Dropdown Structure -->
            <ul id='rawDataDropdown' class='dropdown-content'>
                <li><a class="truncate" href="?rawAnimal">Animal</a></li>
                <li><a class="truncate" href="?rawSpecies">Species</a></li>
                <li><a class="truncate" href="?rawUsers">Users</a></li>
                <li><a class="truncate" href="?rawMortality">Mortality</a></li>
                <li><a class="truncate" href="?rawRehoming">Rehoming</a></li>
                <li><a class="truncate" href="?rawWeightMeasurement">Weight Measurement</a></li>
                <li><a class="truncate" href="?rawLengthMeasurement">Length Measurement</a></li>
                <li><a class="truncate" href="?rawCleanedMeasurement">Cleaned Measurement</a></li>
                <li><a class="truncate" href="?rawFedMeasurement">Fed Measurement</a></li>
                <li><a class="truncate" href="?rawTreatmentMeasurement">Treatment Measurement</a></li>
                <li><a class="truncate" href="?rawKinds">Kinds</a></li>
            </ul>
            <li><p> </p></li>
            <li>
                <a class='dropdown-trigger btn' data-target='rawDataDropdown'>RawData</a>
            </li>
            <?php endif; ?>

        </ul>

        <main>
            <?php
            function handleIllegalPageAccess() {
                global $db;
                global $isAdmin;
                if (!$isAdmin) {
                    $db->log("Tried to access the admin settings page without having the correct permission to", 3);
                    require 'partials/settings/401.php';
                    exit;
                }
            }

                 if (isset($_GET['theme'])) { umami('settings/theme'); include 'partials/settings/user/theme.php'; }
            else if (isset($_GET['about'])) { umami('settings/about'); include 'partials/settings/user/about.php'; }

            else if (isset($_GET['rawAnimal']))     { handleIllegalPageAccess(); include 'partials/functions/raw.php'; generateTable("Animal"); }
            else if (isset($_GET['rawSpecies']))    { handleIllegalPageAccess(); include 'partials/functions/raw.php'; generateTable("Species"); }
            else if (isset($_GET['rawUsers']))      { handleIllegalPageAccess(); include 'partials/functions/raw.php'; generateTable("users"); }
            else if (isset($_GET['rawMortality']))  { handleIllegalPageAccess(); include 'partials/functions/raw.php'; generateTable("mortality"); }
            else if (isset($_GET['rawRehoming']))   { handleIllegalPageAccess(); include 'partials/functions/raw.php'; generateTable("rehoming"); }
            else if (isset($_GET['rawWeightMeasurement']))      { handleIllegalPageAccess(); include 'partials/functions/raw.php'; generateTable("WeightMeasurement"); }
            else if (isset($_GET['rawLengthMeasurement']))      { handleIllegalPageAccess(); include 'partials/functions/raw.php'; generateTable("LengthMeasurement"); }
            else if (isset($_GET['rawCleanedMeasurement']))     { handleIllegalPageAccess(); include 'partials/functions/raw.php'; generateTable("CleanedMeasurement"); }
            else if (isset($_GET['rawFedMeasurement']))         { handleIllegalPageAccess(); include 'partials/functions/raw.php'; generateTable("FedMeasurement"); }
            else if (isset($_GET['rawTreatmentMeasurement']))   { handleIllegalPageAccess(); include 'partials/functions/raw.php'; generateTable("TreatmentMeasurement"); }
            else if (isset($_GET['rawKinds']))   { handleIllegalPageAccess(); include 'partials/functions/raw.php'; generateTable("kindsList"); }

            else if (isset($_GET['general']))   { handleIllegalPageAccess(); include 'partials/settings/admin/general.php'; }
            else if (isset($_GET['users']))     { handleIllegalPageAccess(); include 'partials/settings/admin/users.php'; }
            else if (isset($_GET['logs']))      { handleIllegalPageAccess(); include 'partials/settings/admin/logs.php'; }
            else if (isset($_GET['export']))    { handleIllegalPageAccess(); include 'partials/settings/admin/export.php'; }
            
            else include 'partials/settings/404.php';

            ?>
        </main>

        <div class="fixed-action-btn">
        <a class="btn-floating btn-large" href=".">
            <i class="material-icons">chevron_left</i>
        </a>
        </div>

        <?php include 'partials/scripts.php' ?>
    </body>
</html>