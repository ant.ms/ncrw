# National Centre for Reptile Welfare (NCRW)

![](.screenshot.png)

## Installation

Docker command I used:
```bash
docker run -d --rm -p 80:80 -e LOG_STDOUT=true -e LOG_STDERR=true -e LOG_LEVEL=debug --name NCRW -v "$(pwd):/var/www/html:Z" fauria/lamp
```

### SQL Structure (important!)

Execute the SQL structure in *TEMPLATE.sql* before launching the App

### Adding more lock-screen pictures

In order to add more login-page pictures, just place them into the */img/loginPage/*. Please note, that the filename (including the fil-extension) can't exceed the length of 25 characters.

## Sources

All images (for the login screen for example, were provided by Alec)

**Code**

- [highlight.js](https://highlightjs.org/)
- [MaterializeCSS](https://materializecss.com/)