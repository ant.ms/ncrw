CREATE DATABASE NCRW;
USE NCRW;

CREATE TABLE WeightMeasurement (
    dateRecorded datetime NOT NULL,
    referenceNumber varchar(20) NOT NULL,
    isApproximate tinyint(1),
    recordedWeight int
);
CREATE TABLE LengthMeasurement (
    dateRecorded datetime NOT NULL,
    referenceNumber varchar(20) NOT NULL,
    isApproximate tinyint(1),
    recordedLength int
);
CREATE TABLE CleanedMeasurement (
    dateRecorded datetime NOT NULL,
    referenceNumber varchar(20) NOT NULL
);
CREATE TABLE FedMeasurement (
    dateRecorded datetime NOT NULL,
    referenceNumber varchar(20) NOT NULL
);
CREATE TABLE TreatmentMeasurement (
    dateRecorded datetime NOT NULL,
    referenceNumber varchar(20) NOT NULL,
    notes varchar(100)
);

CREATE Table Species (
    speciesId int NOT NULL AUTO_INCREMENT,
    category tinyint,
    speciesCommonName varchar(40),
    speciesName varchar(50),
    cites varchar(5),
    dwaa tinyint(1),
    ias tinyint(1),
    isNative tinyint(1),
    PRIMARY KEY (speciesId)
);
 
CREATE TABLE Animal (
    ncrwRefrenceNumber varchar(20) NOT NULL,
    isThirdParty tinyint(1),

    -- Both
    species int,
    has_article10 tinyint(1),
    has_giftCert tinyint(1),
    personDonor varchar(25),
    personName varchar(80),
    personEmail varchar(120),
    personPhone varchar(120),
    country char(2),

    animalName varchar(20),
    animalNumberCount1 tinyint,
    animalNumberCount2 tinyint,
    room varchar(50),
    rack varchar(50),
    morph varchar(80),
    sex tinyint,
    source varchar(15),
    postcode varchar(10),
    dateOfBirth datetime,
    dateOfEntry datetime,
    referral varchar(40),
    mites tinyint(1),
    bodyCondition varchar(10),
    mbd varchar(11),
    microchip varchar(40),
    notes text,
    biterisk tinyint,
    confidence tinyint,
    euthanised tinyint(1),
    background varchar(200),

    -- Third party
    origin varchar(20),

    -- Owner
    reasonForRehoming varchar(80),
    durationOwned int,

    createdBy varchar(25),
    PRIMARY KEY (ncrwRefrenceNumber)
);

CREATE TABLE rehoming (
    referenceNumber varchar(20) NOT NULL,
    newOwnerName varchar(30),
    newOwnerContactDetails varchar(100),
    existingConditions varchar(100),
    dateOfDeparture datetime,
    countryCode varchar(2),
    checklist tinyint(1),
    donation tinyint,
    username varchar(25)
);

CREATE TABLE mortality (
    referenceNumber varchar(20) NOT NULL,
    dateOfDeath datetime,
    deathMethod tinyint(1),
    dateOfDeparture datetime,
    causeOfDeath varchar(80),
    vetenerinaryCare tinyint(1),
    postMortem tinyint(1),
    cadaverStored tinyint(1),
    notes varchar(200),
    username varchar(25)
);

CREATE TABLE users (
    username varchar(25) NOT NULL,
    firstname varchar(64),
    lastname varchar(64),
    passwd varchar(64),
    is_administrator tinyint(1),
    ui_accent varchar(5),
    ui_theme varchar(5),
    ui_grouping tinyint(1),
    PRIMARY KEY (username)
);

CREATE TABLE logs (
    dateLogged datetime NOT NULL,
    importance tinyint,
    content varchar(200),
    username varchar(25)
);

CREATE TABLE globalSettings (
    login_bgPicture varchar(25), 
    login_fgPicture varchar(25)
);

CREATE TABLE kindsList (
    section tinyint,
    species int
);

-- change login details if needed (make sure to also change them in partials/database.php)
GRANT ALL ON NCRW.* TO NCRWApp@'127.0.0.1' IDENTIFIED BY 'changeMe';
FLUSH privileges;

INSERT INTO globalSettings (login_bgPicture, login_fgPicture) VALUES ('three.jpg', 'six.jpg');
INSERT INTO users (username, firstname, lastname, passwd, is_administrator, ui_accent, ui_theme) VALUES ("ConfusedAnt", "Yanik", "Ammann", "8a92d19504dc3c3e61f357bec071ab08850d90536e904749acd465e07e8e5f27", 1, 'amber', 'dark');
