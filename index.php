<?php 
	require 'partials/startup.php';
	require 'partials/functions/umami.php';
	require 'partials/functions/generateMenuEntry.php';

	if (isset($_GET['logout'])) {
		require 'partials/login.php';
		exit();
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>National Centre for Reptile Welfare (NCRW)</title>

		<?php include 'partials/head.php' ?>

		<link rel="stylesheet" href="css/_pages.css">
	</head>

	<body>
		<div id="wrapper">
		<?php include 'partials/navbar.php'; ?>

		<ul id="slide-out" class="sidenav sidenav-fixed z-depth-5">
			<li><h4 class="sidebarTitle" onclick="location.href='.'" style="cursor: pointer">NCRW</h4></li>

			<!-- <li><h5 class="sidebarTitle">Edit</h4></li> -->
			<?php
				genMenuEntry("Animals",		"animals");
				genMenuEntry("Species",		"species");

				genSidebarTitle("View");
				genMenuEntry("Animals in",	"animalsIn");
				genMenuEntry("Reasons",		"reasons");
				genMenuEntry("Kinds",		"kinds");
				genMenuEntry("All Time",	"allTime");
				genMenuEntry("Month",		"month");
				genMenuEntry("Year",		"year");
			?>
		</ul>

		<main>
			<?php

			if (isset($_GET['page'])) {
				switch ($_GET['page']) {
					case 'edit':
						if ($readOnly)
							require 'partials/settings/401.php';
						else {
							if (isset($_GET['animal']))         include 'partials/edit/animal.php';
							else if (isset($_GET['species']))   include 'partials/edit/species.php';
						}
						break;
			
					default:
						require 'partials/settings/404.php';
						break;
				}
			}
			else if (isset($_GET['view'])) 			{ umami('animal'); 			include 'partials/view/animal.php'; }
			else if (isset($_GET['animals'])) 		{ umami('animals'); 		include 'partials/list/animals.php'; }
			else if (isset($_GET['species'])) 		{ umami('species'); 		include 'partials/list/species.php'; }
			
			else if (isset($_GET['animalsIn'])) 	{ umami('view/animals-in'); include 'partials/view/animalsIn.php'; }
			else if (isset($_GET['reasons'])) 		{ umami('reasons'); 		include 'partials/view/reasons.php'; }
			else if (isset($_GET['kinds'])) 		{ umami('kinds'); 			include 'partials/view/kinds.php'; }
			else if (isset($_GET['allTime'])) 		{ umami('view/allTime'); 	include 'partials/view/summary-allTime.php'; }
			else if (isset($_GET['month'])) 		{ umami('view/month'); 		include 'partials/view/summary-month.php'; }
			else if (isset($_GET['year'])) 			{ umami('view/year'); 		include 'partials/view/summary-year.php'; }

			else { umami(); include 'partials/dashboard.php'; }
			?>
		</main>
		</div>

		<!-- Floating Button -->
		<div class="fixed-action-btn">
			<a id="floatingButton" class="btn-floating btn-large" href="settings.php?about">
				<i class="material-icons">settings</i>
			</a>
			<ul>
				<li><a class="btn-floating tooltipped" data-position="left" data-tooltip="Logout" href=".?logout"><i class="material-icons">exit_to_app</i></a></li>
			</ul>
		</div>

		<?php include 'partials/scripts.php' ?>
	</body>
</html>