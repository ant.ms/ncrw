checkLength = (element, checkForOnlyCharacters, maxLength, minLength) => {
    if (element.value.length > maxLength || element.value.length < minLength || ((checkForOnlyCharacters) ? !element.value.match("^[A-Za-z0-9]+$") : false)) {
        // Something is wrong
        element.classList.remove('valid');
        element.classList.add('invalid');
    }
    else {
        // Everything seems to be okay
        element.classList.remove('invalid');
        element.classList.add('valid');
    }
}

const copyElementToClipboard = (element) => {
    element.select();
    document.execCommand('copy');
    M.toast({html: 'Successfully copied to clipboard'})
 }

const downloadStringAsFile = (text) => {
    var element = document.createElement('a');

    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', `NCRW-${(new Date().toISOString()).slice(0, 10)}.sql`);

    document.body.appendChild(element);        
    element.click();
    document.body.removeChild(element);
}


const getElement = id => { return document.getElementById(id) }
const getElements = i =>  { return document.querySelectorAll(i) }
const getContent = id => { return document.getElementById(id).value }

const addJS2Dom = (source) => {
    var script = document.createElement('script'); 
    script.src = source; 
    document.head.appendChild(script) ;
}